<?php

namespace App\Http\Controllers;

use App\Http\Requests\AttributeRequest;
use App\Http\Requests\EditAttributeRequest;
use App\Models\Attribute;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class AttributeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $attribute = Attribute::query();
        if (isset($_GET['search'])&&!empty($_GET['search']))
        {
            $search = $request->get('search');
            $attribute->where(function($query) use($search){
                $query->where('name','like','%'.$search.'%');
            });
        }
        $attribute = $attribute->paginate(PER_PAGE);
        return view('attribute.index',compact('attribute'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('attribute.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AttributeRequest $request)
    {
        //
        $attribute =new Attribute();
        $attribute->fill($request->all())->save();
        return redirect()->route('attribute.index');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $attribute =Attribute::find($id);
        if ($attribute)
        {
            return view('attribute.edit',compact('attribute'));
        }
        return redirect()->route('attribute.index')->withErrors('Id is not found');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditAttributeRequest $request, $id)
    {
        //
        $attribute = Attribute::find($id);
        if ($attribute)
        {
            $attribute->fill($request->all())->save();
            return redirect()->route('attribute.index')->withSuccess('Updated successfully!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        try {
            DB::transaction(function () use($id){
                $attribute = Attribute::find($id);
                $attribute->categories()->detach();
                $attribute->delete();
            });
        }catch (\Exception $e){
            return redirect()->route('attribute.index')->withErrors('Can not delete');
        }
        return redirect()->route('attribute.index')->withSuccess('Successfully Deleted');

    }
}
