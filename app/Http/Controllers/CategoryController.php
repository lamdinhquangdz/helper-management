<?php

namespace App\Http\Controllers;

use App\Http\Requests\EditCategoryRequest;
use App\Models\Attribute;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Http\Requests\CategoryRequest;
use Illuminate\Support\Facades\DB;
class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $category = Category::query();
        if (isset($_GET['search'])&&!empty($_GET['search']))
        {
            $search = $request->get('search');
            $category->where(function($query) use($search){
                $query->where('name','like','%'.$search.'%');
            });
        }
        $category = $category->paginate(PER_PAGE);
        return view('category.index',compact('category'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryRequest $request)
    {
        //
        try {
            DB::transaction(function () use($request){
                $category = new Category();
                $data = $request->all();
                $data['slug']=Str::slug($request->name);
                $category->fill($data)->save();
                foreach ($data['attribute'] as $attr) {
                    $attr['category_id'] = $category->id;
                    $category->attributes()->create($attr);
                }
            });
        }catch (\Exception $e){
            return redirect()->route('category.index')->withError('New successfully failed!');
        }
        return redirect()->route('category.index')->withSuccess('New successfully created!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $category = Category::find($id);
        $attribute = Attribute::all();
        if ($category)
        {
            return view('category.edit',compact('category','attribute'));
        }
        return redirect()->route('category.index')->withError('Id was not found');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditCategoryRequest $request, $id)
    {
        //
        try {
            DB::transaction(function () use($request,$id){
                $category = Category::find($id);
                $data = $request->all();
                $data['slug']=Str::slug($request->name);
                $category->fill($data)->save();
                foreach ($data['attribute'] as $attr){
                    $attr['category_id'] =  $category->id;
                    if (isset($attr['id'])){
                        $attr['option']=json_encode($attr['option']);
                       $category->attributes()->where('id',$attr['id'])->update($attr);
                    }else{
                        $category->attributes()->create($attr);
                    }
                }
            });
        }catch(\Exception $e){
            dd($e->getMessage());
            return redirect()->route('category.index')->withError('Updated failed');
        }
        return redirect()->route('category.index')->withSuccess('Successfully Updated');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        try {
            DB::transaction(function () use ($id){
                $category=Category::find($id);
                $category->delete();
            });
        }catch (\Exception $e){
            return redirect()->route('category.index')->withError('Can not delete');
        }
        return redirect()->route('category.index')->withSuccess('Successfully Deleted');
    }
}
