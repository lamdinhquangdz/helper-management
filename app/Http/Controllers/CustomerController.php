<?php

namespace App\Http\Controllers;

use App\Http\Requests\CusEmpRequest;
use App\Http\Requests\EditCusEmplRequest;
use App\Mail\OrderShipped;
use App\Models\District;
use App\Models\Province;
use App\Models\Role;
use App\Models\Ward;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $user = User::where('type','customer')->paginate(PER_PAGE);
        return view('customer.index',compact('user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $role =Role::all();
        $province = Province::all();
        $district= District::all();
        $ward = Ward::all();
        return view('customer.create',compact('role','province','district','ward'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CusEmpRequest $request)
    {
        //

        $data = $request->all();
        $user = new User();
        $random = Str::random(8);
        $data['password']=bcrypt($random);
        Mail::to($request->email)->send( new OrderShipped($random));
        $user ->fill($data)->save();
        return redirect()->route('customer.index')->withSuccess('New successfully created!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $user = User::find($id);
        $provinces = Province::all();
        $district= District::all();
        $ward = Ward::all();
        if($user)
        {
            $role =Role::all();
            return view('customer.edit',compact('role','user','provinces','district','ward'));
        }else
        {
            return  redirect()->route('customer.index')->withErrors('Id not found');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditCusEmplRequest $request, $id)
    {
        //
        $user = User::find($id);
        if($user)
        {
            $data = $request->all();
            $data['password']=bcrypt($request->password);
            $user->fill($data)->save();
            return redirect()->route('customer.index')->withSuccess('Successfully Updated');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $user = User::find($id);
        if ($user)
        {
            $user->delete();
            return redirect()->route('customer.index')->withSuccess('Deleted Successfully');
        }
        return redirect()->route('customer.index')->withErrors('Can not delete');
    }
    public function getDistrict(Request $request)
    {
        $districts = District::where('province_id',$request->province_id)->get();
        return response()->json($districts);
    }

    public function getWards(Request $request)
    {
        $wards = Ward::where('district_id', $request->district_id)->get();
        return response()->json($wards);
    }

    public function search(Request $request)
    {
        $user = User::query();
        $user->where('type','customer');

        if (isset($_GET['search'])&&!empty($_GET['search']))
        {
            $search = $request->get('search');
            $user->where(function($query) use($search){
                $query->where('username','like','%'.$search.'%')
                    ->orwhere('fullname','like','%'.$search.'%')
                    ->orWhere('email','like','%'.$search.'%');
            });

        }
        $user = $user->paginate(PER_PAGE);
        return view('customer.index',compact('user'));
    }
}
