<?php

namespace App\Http\Controllers;

use App\Models\Attribute;
use App\Models\AttributeJob;
use App\Models\Category;
use App\Models\District;
use App\Models\History;
use App\Models\Job;
use App\Models\Province;
use App\Models\Ward;
use App\User;
use Illuminate\Http\Request;
use App\Http\Requests\JobRequest;
use App\Http\Requests\EditJobRequest;
use Illuminate\Support\Facades\DB;
class JobController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $job = Job::query();
        $user = User::all();
        if (isset($request['search'])&&!empty($request['search']))
        {
            $search = $request->get('search');
            $job->where(function($query) use($search){
                $query->where('title','like','%'.$search.'%')
                    ->orWhere('detail','like','%'.$search.'%');
            });

        }
        if (isset($request['status'])&&!empty($request['status'])){
            $job->where('status',$request['status']);
        }
        if (auth()->user()->type == EMPLOYEE){
            $job = Job::where('employee_id',auth()->user()->id);
        }
        if (auth()->user()->type == CUSTOMER){
            $job = Job::where('user_id',auth()->user()->id);
        }
        $job = $job->paginate(PER_PAGE);
        return view('job.index',compact('job','user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $category=Category::all();
        $attribute=Attribute::all();
        $province=Province::all();
        $district=District::all();
        $ward=Ward::all();
        return view('job.create',compact('category','attribute','province','district','ward'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(JobRequest $request)
    {
        try {
            DB::transaction(function() use ($request){
                $job = new Job();
                $data = $request->all();
                $data['user_id']=auth()->user()->id;
                $data['status']='pendding';
                $dataAtt = $request['attribute'];
                $job->fill($data)->save();
                foreach ($dataAtt as $key => $value)
                {
                    $value = json_encode($value);
                    $attribute[$key] = ['value' => $value];
                }
                $job ->attributes()->sync($attribute);
            });
        }catch (\Exception $e){
            return redirect()->route('job.index')->withError('New successfully failed!');
        }
        return redirect()->route('job.index')->withSuccess('New successfully created!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $job = Job::find($id);
        if ($job)
        {
            $user = User::where('type',EMPLOYEE)->get();
            $category = Category::all();
            $provinces = Province::all();
            $district = District::all();
            $attJob =AttributeJob::all();
            $ward = Ward::all();
            $attribute=Attribute::all();
            return view('job.edit',compact('job','category','provinces','district','ward','attribute','user','attJob'));
        }
        return redirect()->route('job.index')->withErrors('Id was not found');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditJobRequest $request, $id)
    {
        //
        try {
            DB::transaction(function () use($request,$id){
                $job = Job::find($id);
                    $data = $request->all();
                    $dataAtt = $request['attribute'];
                    if ($dataAtt!=null)
                    {
                        foreach ($dataAtt as $key => $value)

                        {
                            $value = json_encode($value);
                            $attribute[$key] = ['value' => $value];
                        }
                        $job->attributes()->sync($attribute);
                    }
                    $job->fill($data)->save();
            });
        }catch (\Exception $e){
            return  redirect()->route('job.index')->withError('Update failed');
        }
        return redirect()->route('job.index')->withSuccess('New successfully updated!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        try {
            DB::transaction(function () use ($id){
                $job = Job::find($id);
                $history = History::where('job_id',$id);
                $history->delete();
                $job->attributes()->detach();
                $job->delete();

            });
        }catch (\Exception $e){
            return redirect()->route('job.index')->withErrors('Can not delete');
        }
        return redirect()->route('job.index')->withSuccess('Successfully Deleted');

    }

    public function getAttribute(Request $request)
    {
        $category = Category::find($request->category_id);
        $attributes = $category->attributes;
        $job = Job::find($request->job_id);
        $attributeValues = [];
        return response()->json(['attributes' => $attributes, 'attributeValues' => $attributeValues]);
    }

    public function getJobAttribute(Request $request)
    {
        $category = Category::find($request->category_id);
        $attributes = $category->attributes;
        $job = Job::find($request->job_id);
        $attributeValues = [];
        foreach ($job->attributes as $attribute){
            $attributeValues[] = $attribute->pivot;
        }
        return response()->json(['attributes' => $attributes, 'attributeValues' => $attributeValues]);
    }


    public function show($id){
        $job = Job::find($id);
        if ($job)
        {
            $user = User::where('type',EMPLOYEE)->get();
            $category = Category::all();
            $attribute=Attribute::all();
            $check = $job->histories;
            return view('job.show',compact('job','category','provinces','district','ward','attribute','user','check'));
        }
    }

    public function history(Request $request,$id)
    {
        try {
            DB::transaction(function () use($request,$id){
                $data = $request->all();
                $job = Job::find($id);
                if ($data['status']=='pendding'){
                    $data['employee_id']= null ;
                }
                $job->fill($data)->save();
                $data['user_id']= auth()->user()->id;
                $job->histories()->create($data);
            });
        }catch (\Exception $e){
            return redirect()->route('job.index')->withErrors('Can not add employee');
        }
        return redirect()->route('job.index')->withSuccess('New successfully created!');
    }

}
