<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use  App\Models\Permission;
use Illuminate\Support\Str;
class PermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $permission = Permission::paginate(PER_PAGE);

        return view('permission.index',compact('permission'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('permission.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $permission = new Permission();
        $data = $request->all();
        $data['slug']=Str::slug($request->name);
        $permission->fill($data)->save();
        return redirect()->route('permission.index')->withSuccess('New successfully created');
    }

    public function search(Request $request)
    {
        $permission = Permission::query();
        if (isset($_GET['search'])&&!empty($_GET['search']))
        {
            $search = $request->get('search');
            $permission->where(function($query) use($search){
                $query->where('name','like','%'.$search.'%');
            });

        }
        $permission = $permission->paginate(PER_PAGE);
        return view('permission.index',compact('permission'));
    }
}
