<?php

namespace App\Http\Controllers;

use App\Models\Permission;
use Illuminate\Http\Request;
use App\Models\Role;
use App\User;
class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $role = Role::paginate(PER_PAGE);
        $permission = Permission::all();
        return view('role.index',compact('role','permission'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $permission = Permission::all();
        return view('role.create',compact('permission'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $data = $request['permission'];
        $role = new Role();
        $role->fill($request->all())->save();
        $role->permissions()->sync($data);
        return redirect()->route('role.index')->withSuccess('Thêm mới thành công');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $role = Role::find($id);

        if($role)
        {
            $permission = Permission::all()->sortBy("name");
            return view('role.edit',compact('role','permission'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $role = Role::find($id);

        if($role)
        {
            $data = $request['permission'];
            $role->fill($request->all())->save();
            $role->permissions()->sync($data);
            return redirect()->route('role.index')->withSuccess('Sửa thành công');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $role = Role::find($id);
        $user = User::where('role_id',$id)->first();


        if($user!=null)
        {
            return redirect()->route('role.index')->withSuccess('Cannot delete because there are account properties');
        }
        elseif(empty($user))
        {
            if($role)
            {
                $role->permission()->detach();

                $role->delete();

                return redirect()->route('role.index')->withSuccess('Deleted successfully');
            }
            return redirect()->route('role.index')->with('Error','Deletion failed');
        }
    }
    public function search(Request $request)
    {
        $role = Role::query();
        if (isset($_GET['search'])&&!empty($_GET['search']))
        {
            $search = $request->get('search');
            $role->where(function($query) use($search){
                $query->where('name','like','%'.$search.'%');
            });

        }
        $role = $role->paginate(PER_PAGE);
        return view('role.index',compact('role'));
    }
}
