<?php

namespace App\Http\Controllers;

use App\Http\Requests\EditUserRequet;
use App\Http\Requests\UserRequest;
use App\Models\District;
use App\Models\Province;
use App\Models\Role;
use App\Models\Ward;
use Illuminate\Http\Request;
use App\User;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $user = User::where('type',ADMIN)->paginate(PER_PAGE);
        return view('user.index',compact('user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $role =Role::all();
        return view('user.create',compact('role'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {

        $data = $request->all();
        $user = new User();
        $data['password']=bcrypt($request->passwords);
        $user ->fill($data)->save();
        return redirect()->route('user.index')->withSuccess('New successfully created!');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $user = User::find($id);
        if($user) {
            $role =Role::all();
            return view('user.edit',compact('role','user'));
        } else {
            return  redirect()->route('user.index')->withErrors('Id not found');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditUserRequet $request, $id)
    {
        //
        $user = User::find($id);
        if($user)
        {
            $data = $request->all();
            $data['password']=bcrypt($request->passwords);
            $user->fill($data)->save();
            return redirect()->route('user.index')->withSuccess('Successfully Updated');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $user = User::find($id);
        if ($user)
        {
            $user->delete();
            return redirect()->route('user.index')->withSuccess('Deleted Successfully');
        }
        return redirect()->route('user.index')->withErrors('Can not delete');
    }
    public function search(Request $request)
    {
        $user = User::query();
        $user->where('type','admin');

        if (isset($_GET['search'])&&!empty($_GET['search']))
        {
            $search = $request->get('search');
            $user->where(function($query) use($search){
                $query->where('username','like','%'.$search.'%')
                    ->orwhere('fullname','like','%'.$search.'%')
                    ->orWhere('email','like','%'.$search.'%');
            });

        }
        $user = $user->paginate(PER_PAGE);
        return view('user.index',compact('user'));
    }
}
