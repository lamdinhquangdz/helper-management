<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AttributeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'name'=>'required|max:150',
            'option'=>'required'
        ];
    }
    public function messages()
    {
        return [
            'name.required'=>'Name is not empty',
            'name.max'=>'Max 150 characters',
            'option.required'=>'Option is not empty',
        ];
    }
}
