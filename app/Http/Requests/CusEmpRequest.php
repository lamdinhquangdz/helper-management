<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CusEmpRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = request()->route()->parameter('id');
        return [
            //
            'username'=>'required|unique:users,username,'.$id,
            'fullname'=>'required',
            'email'=>'required|email|unique:users,email,'.$id,
            'phone'=>'required|numeric|min:9',
            'birthday'=>'required',
            'province'=>'required',
            'district'=>'required',
            'ward_id'=>'required'
        ];
    }
    public function messages()
    {
        return [
            'username.required'=>'Username is not empty',
            'username.unique'=>'Username already exists',
            'fullname.required'=>'Fullname is not empty',
            'email.required'=>'Email is not empty',
            'email.email'=>'Email format is not correct',
            'email.unique'=>'Email already exists',
            'phone.required'=>'Phone is not empty',
            'phone.numeric'=>'Phone must be number',
            'phone.min'=>'Phone must have 10 characters',
            'birthday.required'=>'Birthday is not empty',
            'province.required'=>'Province is not empty',
            'district.required'=>'District is not empty',
            'ward_id.required'=>'Ward is not empty'
        ];
    }
}
