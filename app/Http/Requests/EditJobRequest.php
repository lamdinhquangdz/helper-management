<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditJobRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'title'=>'required|max:150',
            'category_id'=>'required',
            'detail'=>'required',
            'province'=>'required',
            'district'=>'required',
            'ward_id'=>'required',
            'home_number'=>'required'
        ];
    }
    public function messages()
    {
        return [
            'title.required'=>'Title is not empty',
            'title.max'=>'Max 150 characters',
            'category_id.required'=>'Category is not empty',
            'detail.required'=>'Detail is not empty',
            'province.required'=>'Province is not empty',
            'district.required'=>'District is not empty',
            'ward_id.required'=>'Ward is not empty',
            'home_number'=>'Home number is not empty'
        ];
    }
}
