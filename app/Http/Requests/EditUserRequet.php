<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditUserRequet extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'username'=>'required',
            'confirm'=>'same:passwords'
        ];
    }
    public function messages()
    {
        return [
            'username.required'=>'Username is not empty',
            'fullname.required'=>'Fullname is not empty'
        ];
    }
}
