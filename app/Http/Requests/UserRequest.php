<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = request()->route()->parameter('id');
        return [
            //
            'username'=>'required|unique:users,username,'.$id,
            'fullname'=>'required',
            'email'=>'required|email|unique:users,email,'.$id,
            'passwords'=>'required|min:6|max:8',
            'confirm'=>'same:passwords'
        ];
    }
    public function messages()
    {
        return [
            'username.required'=>'Username is not empty',
            'username.unique'=>'Username already exists',
            'fullname.required'=>'Fullname is not empty',
            'email.required'=>'Email is not empty',
            'email.email'=>'Email format is not correct',
            'email.unique'=>'Email already exists',
            'passwords.required'=>'Password is not empty',
            'passwords.min'=>'Password has 6 to 8 characters',
            'passwords.max'=>'Password has 6 to 8 characters',
            'confirm.same'=>'Password do not match'
        ];
    }
}
