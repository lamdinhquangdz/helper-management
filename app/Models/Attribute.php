<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Attribute extends Model
{
    protected $table = 'attributes';
    protected $fillable=[
        'name',
        'option',
        'type',
        'category_id'
    ];

    protected $casts=[
        'option'=> 'array',
    ];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function jobs()
    {
        return $this->belongsToMany(Job::class,'attribute_job','attribute_id','job_id');
    }
}
