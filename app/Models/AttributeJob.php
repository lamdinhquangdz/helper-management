<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AttributeJob extends Model
{
    //
    protected $table = 'attribute_job';

    protected $casts = [
      'value' => 'array'
    ];
}
