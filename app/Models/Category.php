<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable=[
        'name',
        'slug'
    ];

    public function job()
    {
        return $this->hasMany(Job::class);
    }

    public function attributes()
    {
        return $this->hasMany(Attribute::class);
    }
}
