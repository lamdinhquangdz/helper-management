<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class History extends Model
{
    //
    protected $table='histories';
    protected $fillable=
        [
        'job_id',
        'user_id',
        'status'
        ];
    public function job()
    {
        return $this->belongsTo(Job::class);
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
