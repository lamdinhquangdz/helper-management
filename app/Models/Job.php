<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    //
    protected $fillable=[
      'title',
      'category_id',
      'detail',
      'status',
      'user_id',
      'ward_id',
      'home_number',
      'employee_id'
    ];

    protected $casts = [
      'value' => 'array'
    ];

    public function ward()
    {
        return $this->belongsTo(Ward::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }
    public function attributes()
    {
        return $this->belongsToMany(Attribute::class,'attribute_job','job_id','attribute_id')->withPivot('value');
    }
    public function histories()
    {
        return $this->hasMany(History::class);
    }
}
