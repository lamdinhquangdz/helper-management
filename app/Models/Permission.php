<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Role;
class Permission extends Model
{
    //
    protected $fillable=[
        'name',
        'slug'
    ];
    public function role()
    {
        return $this->belongsToMany(Role::class,'permission_id','role_id');
    }
}
