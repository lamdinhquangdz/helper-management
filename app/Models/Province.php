<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    //

    public function districts()
    {
        return $this->hasMany(District::class);
    }
    public function users()
    {
        return $this->hasMany(User::class);
    }
}
