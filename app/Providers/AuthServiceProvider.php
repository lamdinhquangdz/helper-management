<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use App\User;
use App\Policies\AdminPolicy;
class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
//         'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
//admin
        Gate::define('view-admin',function ($user){
            return $user->checkPemissionAccess('view-admin');
        });
        Gate::define('create-admin',function ($user){
            return $user->checkPemissionAccess('create-admin');
        });
        Gate::define('edit-admin',function ($user){
            return $user->checkPemissionAccess('edit-admin');
        });
        Gate::define('delete-admin',function ($user){
            return $user->checkPemissionAccess('delete-admin');
        });
//customer
        Gate::define('view-customer',function ($user){
            return $user->checkPemissionAccess('view-customer');
        });
        Gate::define('create-customer',function ($user){
            return $user->checkPemissionAccess('create-customer');
        });
        Gate::define('edit-customer',function ($user){
            return $user->checkPemissionAccess('edit-customer');
        });
        Gate::define('delete-customer',function ($user){
            return $user->checkPemissionAccess('delete-customer');
        });
//employee
        Gate::define('view-employee',function ($user){
            return $user->checkPemissionAccess('view-employee');
        });
        Gate::define('create-employee',function ($user){
            return $user->checkPemissionAccess('create-employee');
        });
        Gate::define('edit-employee',function ($user){
            return $user->checkPemissionAccess('edit-employee');
        });
        Gate::define('delete-employee',function ($user){
            return $user->checkPemissionAccess('delete-employee');
        });
//role
        Gate::define('view-role',function ($user){
            return $user->checkPemissionAccess('view-role');
        });
        Gate::define('create-role',function ($user){
            return $user->checkPemissionAccess('create-role');
        });
        Gate::define('edit-role',function ($user){
            return $user->checkPemissionAccess('edit-role');
        });
        Gate::define('delete-role',function ($user){
            return $user->checkPemissionAccess('delete-role');
        });
//permission
        Gate::define('view-permission',function ($user){
            return $user->checkPemissionAccess('view-permission');
        });
//category
        Gate::define('view-category',function ($user){
            return $user->checkPemissionAccess('view-category');
        });
        Gate::define('create-category',function ($user){
            return $user->checkPemissionAccess('create-category');
        });
        Gate::define('edit-category',function ($user){
            return $user->checkPemissionAccess('edit-category');
        });
        Gate::define('delete-category',function ($user){
            return $user->checkPemissionAccess('delete-category');
        });
//attribute
        Gate::define('view-attribute',function ($user){
            return $user->checkPemissionAccess('view-attribute');
        });
        Gate::define('create-attribute',function ($user){
            return $user->checkPemissionAccess('create-attribute');
        });
        Gate::define('edit-attribute',function ($user){
            return $user->checkPemissionAccess('edit-attribute');
        });
        Gate::define('delete-attribute',function ($user){
            return $user->checkPemissionAccess('delete-attribute');
        });
//job
        Gate::define('view-job',function ($user){
            return $user->checkPemissionAccess('view-job');
        });
        Gate::define('create-job',function ($user){
            return $user->checkPemissionAccess('create-job');
        });
        Gate::define('edit-job',function ($user){
            return $user->checkPemissionAccess('edit-job');
        });
        Gate::define('delete-job',function ($user){
            return $user->checkPemissionAccess('delete-job');
        });
    }
}
