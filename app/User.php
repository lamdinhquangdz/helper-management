<?php

namespace App;

use App\Models\History;
use App\Models\Job;
use App\Models\Province;
use App\Models\Role;
use App\Models\Ward;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    use SoftDeletes;

    protected $fillable = [
        'username', 'email', 'password', 'fullname',
        'phone', 'detail', 'birthday',
        'role_id', 'ward_id', 'type'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function ward()
    {
        return $this->belongsTo(Ward::class);
    }

    public function jobs()
    {
        return $this->hasMany(Job::class);
    }

    public function histories()
    {
        return $this->hasMany(History::class);
    }

    public function checkPemissionAccess($check)
    {

        $roles = auth()->user()->role;
        if (!empty($roles)) {
            $permissions = $roles->permissions;
            if ($permissions->contains('slug', $check)) {
                return true;
            }
            return false;
        } else {
            $type = auth()->user()->type;
            if($type == EMPLOYEE){
                switch ($check){
                    case 'view-employee':
                    case 'create-employee':
                    case 'edit-employee':
                    case 'delete-employee':
                    case 'view-job':
                        return true;
                    default:
                        return false;
                }
            }
            if($type == CUSTOMER){
                switch ($check){
                    case 'view-customer':
                    case 'create-customer':
                    case 'edit-customer':
                    case 'delete-customer':
                    case 'view-job':
                    case 'create-job':
                    case 'edit-job':
                    case 'delete-job':
                        return true;
                    default:
                        return false;
                }
            }
            return false;
        }
    }
}
