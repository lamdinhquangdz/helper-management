<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('username');
            $table->string('email')->unique();
            $table->string('fullname');
            $table->string('password');
            $table->string('type');
            $table->timestamp('email_verified_at')->nullable();
            $table->integer('phone')->nullable();
            $table->string('detail')->default('null');
            $table->integer('birthday')->nullable();
            $table->unsignedBigInteger('role_id');
            $table->unsignedBigInteger('ward_id');
            $table->foreign('role_id')->references('id')->on('roles');
            $table->foreign('ward_id')->references('id')->on('wards');
            $table->softDeletes();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
