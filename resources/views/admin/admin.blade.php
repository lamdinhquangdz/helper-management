@extends('master.main')
@section('content')
    @if (session()->has('success'))
        <div class="alert alert-success">
            @if(is_array(session('success')))
                @foreach (session('success') as $message)
                    {{ $message }}
                @endforeach
            @else
                {{ session('success') }}
            @endif
        </div>
    @endif

    <h1 class="text-center">Welcome to Helper Management</h1>
@endsection
