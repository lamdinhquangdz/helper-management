@extends('master.main')
@section('content')
    <div class=" text-center loginscreen animated fadeInDown">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-title">
                        <h5>Create attribute</h5>
                        <a class="btn aa btn-primary " type="button" href="{{route('attribute.index')}}"><i class="fa fa-list"></i>&nbsp;List</a>
                    </div>
                    <div class="ibox-content">
                        <form id="form" method="post" action="{{ route('attribute.store') }}" class="wizard-big">
                            @csrf
                            <fieldset>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Name</label>
                                            <input value="{{old('name')}}" id="name" name="name" type="text" class="form-control @error('name') is-invalid @enderror">
                                        </div>
                                        @error('name')
                                        <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                        @enderror
                                    </div>

                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Type</label>
                                            <select class="form-control m-b" name="type" >
                                                <option value="checkbox">checkbox</option>
                                                <option value="text">text</option>
                                                <option value="radio">radio</option>
                                                <option value="textarea">textarea</option>
                                                <option value="select">select</option>
                                                <option value="select-multi">select multi</option>
                                            </select>
                                            @error('type')
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Option</label>
                                        </div>
                                            <table border="0" id="dynamic_field" class="col-lg-12 mgl " >
                                                <tr class="space">
                                                    <td class="col-lg-11 ">
                                                        <input type="text" value="" id="option" name="option[]" placeholder="Enter your option" class="form-control option_list @error('option') is-invalid @enderror required" />
                                                    </td>
                                                    <td class="col-lg-1">
                                                        <button type="button" name="add" id="add" class="btn btn-primary "><i class="fa fa-plus"></i></button>
                                                    </td>
                                                </tr>
                                            </table>
                                            @error('option')
                                            <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                            @enderror
                                            <p class="help is-danger"  style="color: red" >{{$errors->first('option.*')}}</p>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-7 m-b-xs">
                                    </div>
                                    <div class="col-sm-5">
                                        <div class="col-sm-8 col-sm-offset-5">
                                            <button class="btn btn-white" type="reset">Reset</button>
                                            <button class="btn btn-primary" type="submit">Save changes</button>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            var i = 1;
            $('#add').click(function () {
                i++;
                $('#dynamic_field').append('<tr class="space" id="row' + i + '"><td class="col-lg-11"><input type="text" name="option[]" placeholder="Enter your Option" class="form-control option_list" /></td><td class="col-lg-1"><button type="button" name="remove" id="' + i + '" class="btn btn-danger btn_remove"><i class="fa fa-times"></i></button></td></tr>');
            });
            $(document).on('click', '.btn_remove', function () {
                var button_id = $(this).attr("id");
                $('#row' + button_id + '').remove();
            });
            // $('#submit').click(function(){
            //     $.ajax({
            //         url:"name.php",
            //         method:"POST",
            //         data:$('#add_name').serialize(),
            //         success:function(data)
            //         {
            //             alert(data);
            //             $('#add_name')[0].reset();
            //         }
            //     });
            // });
        });
    </script>
@endsection




