@extends('master.main')
@section('content')
    <div class=" text-center loginscreen animated fadeInDown">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-title">
                        <h5>Edit attribute</h5>
                        <a class="btn aa btn-primary " type="button" href="{{route('attribute.index')}}"><i class="fa fa-list"></i>&nbsp;List</a>
                    </div>
                    <div class="ibox-content">
                        <form id="form" method="post" action="{{ route('attribute.update',['id'=>$attribute->id])}}" class="wizard-big">
                            @csrf
                            <fieldset>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Name</label>
                                            <input value="{{$attribute->name}}" id="name" name="name" type="text" class="form-control @error('name') is-invalid @enderror">
                                        </div>
                                        @error('name')
                                        <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                        @enderror
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Option</label>
                                        </div>
                                        <div class="row">
                                            @foreach($attribute->option as $item)
                                                <div class="col-lg-4 form-group form-inline">
                                                    <input type="text" value="{{$item}}" id="row" name="option[]" placeholder="Enter your option" class="form-control option_list required"/>
                                                    @if($loop->first)
                                                        <td class="col-lg-1">
                                                            <button type="button" name="add" id="add" class="btn btn-primary "><i class="fa fa-plus"></i></button>
                                                        </td>
                                                    @endif
                                                    <button type="button" name="remove"  class="btn btn-danger btn_remove"><i class="fa fa-times"></i></button>
                                                </div>
                                            @endforeach

                                        </div>
                                        <div id="dynamic_field" class="col-lg-4 form-group form-inline" >

                                        </div>
                                        <p class="help is-danger"  style="color: red" >{{$errors->first('option.*')}}</p>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-7 m-b-xs">
                                    </div>
                                    <div class="col-sm-5">
                                        <div class="col-sm-8 col-sm-offset-5">
                                            <button class="btn btn-white" type="reset">Reset</button>
                                            <button class="btn btn-primary" type="submit">Save changes</button>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            var i = 1;
            $('#add').click(function () {
                i++;
                $('#dynamic_field').append('<input type="text" name="option[]" placeholder="Enter your Option"' +
                    ' id="row' + i + '" class="form-control option_list" />' +
                    '<td class="col-lg-1">' +
                    '<button type="button"' +
                    ' name="remove" id="' + i + '" class="btn btn-danger btn_remove"><i class="fa fa-times"></i>' +
                    '</button>' +
                    '</td>'

                );

            });
            $(document).on('click', '.btn_remove', function () {
                var button_id = $(this).attr("id");
                $('#row' + button_id + '').remove();
                $(this).closest('.btn_remove').remove();
            });
        });
    </script>
@endsection





