@extends('master.main')
@section('content')
    @if (session()->has('success'))
        <div class="alert alert-success">
            @if(is_array(session('success')))
                @foreach (session('success') as $message)
                    {{ $message }}
                @endforeach
            @else
                {{ session('success') }}
            @endif
        </div>
    @endif
    @if (session()->has('error'))
        <div class="alert alert-danger">
            @if(is_array(session('error')))
                @foreach (session('error') as $message)
                    {{ $message }}
                @endforeach
            @else
                {{ session('error') }}
            @endif
        </div>
    @endif
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Attribute </h5>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-sm-9 m-b-xs">
                            @can('view-attribute')
                            <a class="btn btn-primary " type="button" href="{{route('attribute.create')}}"><i class="fa fa-plus"></i>&nbsp;Add</a>
                            @endcan
                        </div>
                        <form action="" method="get">
                            <div class="col-sm-3">
                                <div class="input-group">
                                    <input type="text" placeholder="Search" name="search" class="input-sm form-control">
                                    <span class="input-group-btn">
                                        <button type="submit" class="btn btn-sm btn-primary"> Go!</button>
                                    </span>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Attribute Name </th>
                                <th>Option</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($attribute as $item)
                                <tr>
                                    <td>{{$item->id}}</td>
                                    <td>{{$item->name}}</td>
                                    <td>
                                        @if($item->option)
                                            @foreach($item->option as $option)
                                                {{$option}}<br/>
                                            @endforeach
                                        @endif
                                    </td>
                                    <td class="col-sm-2 row">
                                        @can('edit-attribute')
                                        <a href="{{route('attribute.edit',['id'=>$item->id])}}" class="btn btn-info bt dim" type="button">
                                            <i class="fa fa-paste"></i>
                                        </a>
                                        @endcan
                                        @can('delete-attribute')
                                        <a href="{{route('attribute.destroy',['id'=>$item->id])}}" class="btn btn-danger bt dim " onclick="return confirm('Có chắc muốn xóa?')" type="submit">
                                            <i class="fa fa-trash-o"></i>
                                        </a>
                                        @endcan
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-sm-5 m-b-xs">
                        </div>
                        <div class="col-sm-7">
                            <div class="input-group">
                                {{$attribute->links()}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection


