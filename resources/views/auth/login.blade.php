@extends('layouts.app')

@section('content')

<body class="gray-bg">

<div class="middle-box text-center loginscreen animated fadeInDown">
    <div>
        <div>

            <h1 class="logo-name">IN+</h1>

        </div>
        <h3>Welcome to IN+</h3>
        <p>Login in. To see it in action.</p>
        <form class="m-t" role="form" method="POST" action="{{ route('login') }}">
            @csrf

            <div class="form-group">
                <input type="email" value="{{old('email')}}" name="email" class="form-control @error('email') is-invalid @enderror" placeholder="Email-address" required>
            </div>
            @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror

            <div class="form-group">
                <input name="password" type="password" class="form-control @error('password') is-invalid @enderror" placeholder="Password" required>
            </div>
            @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror

            <button type="submit" class="btn btn-primary block full-width m-b">Login</button>
            @if (Route::has('password.request'))
                <a href="{{ route('password.request') }}"><small>Forgot password?</small></a>
            @endif

            <p class="text-muted text-center"><small>Do not have an account?</small></p>

            @if (Route::has('register'))
                <a class="btn btn-sm btn-white btn-block" href="{{ route('register') }}">Create an account</a>
            @endif
        </form>
    </div>
</div>

<!-- Mainly scripts -->
<script src="{{asset('backend/js/jquery-3.1.1.min.js')}}"></script>
<script src="{{asset('backend/js/bootstrap.min.js')}}"></script>

</body>
@endsection
