@extends('layouts.app')

@section('content')
<body class="gray-bg ">

<div class=" text-center loginscreen animated fadeInDown">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ibox">
                        <div class="ibox-title">
                            <h5>Create account</h5>
                        </div>
                        <div class="ibox-content">
                            <form id="form" method="post" action="{{ route('register') }}" class="wizard-big">
                                @csrf
                                <fieldset>
                                    <h2>Account Information</h2>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Username *</label>
                                                <input value="{{old('username')}}" id="username" name="username" type="text" class="form-control @error('username') is-invalid @enderror required">
                                            </div>
                                            @error('username')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror

                                            <div class="form-group">
                                                <label>Fullname *</label>
                                                <input value="{{old('fullname')}}" id="fullName" name="fullname" type="text" class="form-control @error('fullname') is-invalid @enderror required">
                                            </div>
                                            @error('fullname')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror

                                            <div class="form-group">
                                                <label>Email *</label>
                                                <input value="{{old('email')}}" id="email" name="email" type="text"  class="form-control @error('email') is-invalid @enderror required">
                                            </div>
                                            @error('email')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>

                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Type *</label>
                                                <select class="form-control m-b @error('type') is-invalid @enderror required" name="type" >
                                                    <option value="customer">customer</option>
                                                    <option value="employee">employee</option>
                                                </select>
                                            </div>
                                            @error('type')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror

                                            <div class="form-group">
                                                <label>Password *</label>
                                                <input value="{{old('password')}}" id="password" name="password" type="password" class="form-control @error('password') is-invalid @enderror required">
                                            </div>
                                            @error('password')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror

                                            <div class="form-group">
                                                <label>Confirm Password *</label>
                                                <input id="confirm" name="confirm" type="password" class="form-control @error('confirm') is-invalid @enderror required">
                                            </div>
                                            @error('confirm')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                </fieldset>

                                <fieldset>
                                    <h2>Profile Information</h2>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Phone *</label>
                                                <input value="{{ old('phone') }}" id="phone" name="phone" type="text" class="form-control @error('phone') is-invalid @enderror required">
                                            </div>
                                            @error('phone')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror



                                        </div>

                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label>Birthday *</label>
                                                <input value="{{old('birthday')}}" id="birthday" name="birthday" type="text" class="form-control @error('birthday') is-invalid @enderror required">
                                            </div>
                                            @error('birthday')
                                            <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror

                                        </div>
                                    </div>
                                </fieldset>
                                <button type="submit" class="btn btn-primary block full-width m-b">Register</button>

                                <p class="text-muted text-center"><small>Already have an account?</small></p>
                                <a class="btn btn-sm btn-white btn-block" href="{{ route('login') }}">Login</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
</div>


<!-- Mainly scripts -->
<script src="{{asset('backend/js/jquery-3.1.1.min.js')}}"></script>
<script src="{{asset('backend/js/bootstrap.min.js')}}"></script>
<!-- iCheck -->
<script src="{{asset('backend/js/plugins/iCheck/icheck.min.js')}}"></script>
<script>
    $(document).ready(function(){
        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });
    });
</script>
<script src="{{asset('backend/js/plugins/metisMenu/jquery.metisMenu.js')}}"></script>
<script src="{{asset('backend/js/plugins/slimscroll/jquery.slimscroll.min.js')}}"></script>

<!-- Custom and plugin javascript -->
<script src="{{asset('backend/js/inspinia.js')}}"></script>
<script src="{{asset('backend/js/plugins/pace/pace.min.js')}}"></script>

<!-- Steps -->
<script src="{{asset('backend/js/plugins/steps/jquery.steps.min.js')}}"></script>

<!-- Jquery Validate -->
<script src="{{asset('backend/js/plugins/validate/jquery.validate.min.js')}}"></script>


<script>
    $(document).ready(function(){
        $("#wizard").steps();
        $("#form").steps({
            bodyTag: "fieldset",
            onStepChanging: function (event, currentIndex, newIndex)
            {
                // Always allow going backward even if the current step contains invalid fields!
                if (currentIndex > newIndex)
                {
                    return true;
                }

                // Forbid suppressing "Warning" step if the user is to young
                if (newIndex === 3 && Number($("#age").val()) < 18)
                {
                    return false;
                }

                var form = $(this);

                // Clean up if user went backward before
                if (currentIndex < newIndex)
                {
                    // To remove error styles
                    $(".body:eq(" + newIndex + ") label.error", form).remove();
                    $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                }

                // Disable validation on fields that are disabled or hidden.
                form.validate().settings.ignore = ":disabled,:hidden";

                // Start validation; Prevent going forward if false
                return form.valid();
            },
            onStepChanged: function (event, currentIndex, priorIndex)
            {
                // Suppress (skip) "Warning" step if the user is old enough.
                if (currentIndex === 2 && Number($("#age").val()) >= 18)
                {
                    $(this).steps("next");
                }

                // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                if (currentIndex === 2 && priorIndex === 3)
                {
                    $(this).steps("previous");
                }
            },
            onFinishing: function (event, currentIndex)
            {
                var form = $(this);

                // Disable validation on fields that are disabled.
                // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                form.validate().settings.ignore = ":disabled";

                // Start validation; Prevent form submission if false
                return form.valid();
            },
            onFinished: function (event, currentIndex)
            {
                var form = $(this);

                // Submit form input
                form.submit();
            }
        }).validate({
            errorPlacement: function (error, element)
            {
                element.before(error);
            },
            rules: {
                confirm: {
                    equalTo: "#password"
                }
            }
        });
    });
</script>
</body>
@endsection
