@extends('master.main')
@section('content')
    <div class=" text-center loginscreen animated fadeInDown">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-title">
                        <h5>Create category</h5>
                        <a class="btn aa btn-primary " type="button" href="{{route('category.index')}}"><i class="fa fa-list"></i>&nbsp;List</a>
                    </div>
                    <div class="ibox-content">
                        <form id="form" method="post" action="{{ route('category.store') }}" class="wizard-big">
                            @csrf
                            <fieldset>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Name</label>
                                            <input value="{{old('name')}}" id="name" name="name" type="text" class="form-control @error('name') is-invalid @enderror ">
                                            @error('name')
                                            <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="add-atrribute">
                                            <div class="parentAttr" data-id="0">
                                                <div class="form-group">
                                                    <label for="Attribute">Attribute (*):</label>
                                                    <input type="text" name="attribute[1][name]" id="Attribute"
                                                           placeholder="Attribute Name" class="form-control" value="{{old('name')}}">
                                                    <p class="help is-danger" style="color: red">{{ $errors->first('name') }}</p>
                                                </div>
                                                <div class="form-group">
                                                    <label for="type">Type (*):</label>
                                                    <select name="attribute[1][type]" id="type" class="form-control">
                                                        <option value="checkbox">Check Box</option>
                                                        <option value="radio">Radio</option>
                                                        <option value="select">Select</option>
                                                        <option value="text">Text</option>
                                                        <option value="select multi">Select multi</option>
                                                        <option value="textarea">Text Area</option>
                                                    </select>
                                                    <p class="help is-danger" style="color: red">{{ $errors->first('type') }}</p>
                                                </div>
                                                <div class="form-group">
                                                    <table class="table-light dynamic_field" id="dynamic_field">
                                                        <label for="option">Value (*):</label>
                                                        <tr class="attr">
                                                            <td>
                                                                <input type="text" name="attribute[1][option][]" id="option"
                                                                       style="margin-bottom: 10px;"
                                                                       placeholder="Value" class="form-control name_list"/>
                                                            </td>
                                                            <td>
                                                                <button type="button" name="add" onclick="addRow()"
                                                                        class="btn btn-primary add"
                                                                        style="margin-bottom: 10px;margin-left: 10px">
                                                                    <i class="fa fa-plus"></i>
                                                                </button>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div class="form-group text-right">
                                                    <h3>Add Attribute</h3>
                                                    <button type="button" name="add" onclick="addAttribute(this)"
                                                            class="btn btn-primary"
                                                            style="margin-bottom: 10px;margin-left: 10px">
                                                        <i class="fa fa-plus"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-7 m-b-xs">
                                    </div>
                                    <div class="col-sm-5">
                                        <div class="col-sm-8 col-sm-offset-5">
                                            <button class="btn btn-white" type="reset">Reset</button>
                                            <button class="btn btn-primary" type="submit">Save changes</button>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <script src="//cdn.ckeditor.com/4.15.1/standard/ckeditor.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script type="text/javascript">
        function addRow() {
            $('.dynamic_field').append('<tr class="attr"><td><input type="text" name="attribute[1][option][]"' +
                ' style="margin-bottom: 10px;" placeholder="Value" class="form-control name_list" /></td>' +
                '<td><button type="button" name="remove" style="margin-bottom: 10px;margin-left: 10px"  class="btn btn-danger btn_remove">X</button>' +
                ' <button type="button" name="add" onclick="addRow()" class="btn btn-primary add" style="margin-bottom: 10px;margin-left: 10px"><i class="fa fa-plus"></i></button></td>' +
                '</tr>');
        }
        $(document).ready(function () {
            $(document).on('click', '.btn_remove', function () {
                $(this).closest('.attr').remove();
            });
        });
        var countAddAtrribute = $(".add-atrribute").length;
        function addAttribute(data) {
            console.log(data.closest('.parentAttr'));
            countAddAtrribute = countAddAtrribute+1;
            $('.add-atrribute').append(
                '<div class="parentAttr">' +
                    '<div class="form-group">' +
                        '<label for="Attribute">Attribute (*):</label>' +
                        '<input type="text" name="attribute['+countAddAtrribute+'][name]" id="Attribute" placeholder="Attribute Name" class="form-control">' +
                    '</div>' +
                    '<div class="form-group">' +
                        '<label for="type">Type (*):</label>' +
                        '<select name="attribute['+countAddAtrribute+'][type]" id="type" class="form-control">' +
                            '<option value="checkbox">Check Box</option>' +
                            '<option value="radio">Radio</option>' +
                            '<option value="select">Select</option>' +
                            '<option value="text">Text</option>' +
                            '<option value="select multi">Select multi</option>' +
                            '<option value="textarea">Text Area</option>' +
                        '</select>' +
                    '</div>' +
                    '<div class="form-group">' +
                        '<table class="table-light dynamic_fields" id="dynamic_field">' +
                            '<label for="option">Value (*):</label>' +
                                '<tr class="attr">' +
                                    '<td>' +
                                        '<input type="text" name="attribute['+countAddAtrribute+'][option][]" id="option" style="margin-bottom: 10px;"' +
                                            'placeholder="Value" class="form-control name_list"/>' +
                                    '</td>' +
                                    '<td> ' +
                                        '<button type="button" name="add" onclick="addRowValue($(this).parent().parent().parent())"' +
                                            'class="btn btn-primary add"' +
                                            'style="margin-bottom: 10px;margin-left: 10px">' +
                                            '<i class="fa fa-plus"></i>' +
                                        '</button>' +
                                    '</td>' +
                                '</tr>' +
                        '</table>' +
                    '</div>' +
                    '<div class="form-group text-right">' +
                        '<h3>Add Attribute</h3>' +
                        '<button type="button" name="remove"' +
                            'style="margin-bottom: 10px;margin-left: 10px"' +
                            'class="btn btn-danger btn_remove_attr" onclick="removeAttr($(this).parent().parent())">X' +
                        '</button>' +
                        '<button type="button" name="add" onclick="addAttribute(this)"' +
                            'class="btn btn-primary"' +
                            'style="margin-bottom: 10px;margin-left: 10px">' +
                            '<i class="fa fa-plus"></i>' +
                        '</button>' +
                    '</div>' +
                '</div>'
            );
        }
        function addRowValue(attr) {
            console.log()
            $(attr).append('<tr class="attr"><td><input type="text" name="attribute['+countAddAtrribute+'][option][]"' +
                ' style="margin-bottom: 10px;" placeholder="Value" class="form-control name_list" /></td>' +
                '<td><button type="button" name="remove" style="margin-bottom: 10px;margin-left: 10px"  class="btn btn-danger btn_removes">X</button>' +
                ' <button type="button" name="add" onclick="addRowValue($(this).parent().parent().parent())" class="btn btn-primary add" style="margin-bottom: 10px;margin-left: 10px"><i class="fa fa-plus"></i></button></td>' +
                '</tr>');
        }
        $(document).ready(function () {
            $(document).on('click', '.btn_removes', function () {
                $(this).closest('.attr').remove();
            });
        });
        function removeAttr(attr) {
            attr.remove();
        }
    </script>
@endsection



