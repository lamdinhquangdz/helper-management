@extends('master.main')
@section('content')
    <div class=" text-center loginscreen animated fadeInDown">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-title">
                        <h5>Create account</h5>
                        <a class="btn aa btn-primary " type="button" href="{{route('category.index')}}"><i class="fa fa-list"></i>&nbsp;List</a>
                    </div>
                    <div class="ibox-content">
                        <form id="form" method="post" action="{{ route('category.update',['id'=>$category->id]) }}" class="wizard-big">
                            @csrf
                            <fieldset>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Name *</label>
                                            <input value="{{$category->name}}" id="name" name="name" type="text" class="form-control @error('name') is-invalid @enderror required">
                                        </div>
                                        @error('name')
                                        <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                        @enderror
                                    </div>
                                    <div class="col-lg-12">
                                        <label for="">Attribute</label>
                                        <div class="add-atrribute">
                                            @foreach($category->attributes as $attr)
                                                <input type="hidden" name="attribute[{{$loop->index}}][id]" value="{{$attr->id}}">
                                                <div class="parentAttr" data-id="0">
                                                    <div class="form-group">
                                                        <label for="Attribute">Attribute (*):</label>
                                                        <input type="text" name="attribute[{{$loop->index}}][name]" id="Attribute"
                                                               placeholder="Attribute Name" class="form-control"
                                                               value="{{old('name') ? old('name'):$attr->name}}">
                                                        <p class="help is-danger" style="color: red">{{ $errors->first('name') }}</p>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="type">Type (*):</label>
                                                        <select name="attribute[{{$loop->index}}][type]" id="type" class="form-control">
                                                            <option value="checkbox" {{$attr->type == 'checkbox' ? 'selected' : ''}}>Check Box</option>
                                                            <option value="radio" {{$attr->type == 'radio' ? 'selected' : ''}}>Radio</option>
                                                            <option value="select" {{$attr->type == 'select' ? 'selected' : ''}}>Select</option>
                                                            <option value="text" {{$attr->type == 'text' ? 'selected' : ''}}>Text</option>
                                                            <option value="select multi" {{$attr->type == 'select multi' ? 'selected' : ''}}>Select multi</option>
                                                            <option value="textarea" {{$attr->type == 'textarea' ? 'selected' : ''}}>Text Area</option>
                                                        </select>
                                                        <p class="help is-danger" style="color: red">{{ $errors->first('type') }}</p>
                                                    </div>
                                                    <div class="form-group">
                                                        <table class="table-light dynamic_field" id="dynamic_field">
                                                            <label for="option">Value (*):</label>
                                                            @foreach($attr->option as $options)
                                                                <tr class="attr">
                                                                    <td>
                                                                        <input type="text" name="attribute[{{$loop->parent->index}}][option][]" id="option"
                                                                               style="margin-bottom: 10px;" value="{{old('option') ? old('option'):$options}}"
                                                                               placeholder="Value" class="form-control name_list"/>
                                                                    </td>
                                                                    <td>
                                                                        <button type="button" name="remove"
                                                                                style="margin-bottom: 10px;margin-left: 10px"
                                                                                class="btn btn-danger btn_remove">X
                                                                        </button>
                                                                        <button type="button" name="add" onclick="addRow()"
                                                                                class="btn btn-primary add"
                                                                                style="margin-bottom: 10px;margin-left: 10px">
                                                                            <i class="fa fa-plus"></i>
                                                                        </button>
                                                                    </td>
                                                                </tr>
                                                            @endforeach
                                                        </table>
                                                    </div>
                                                    <div class="form-group text-right">
                                                        <h3>Add Attribute</h3>
                                                        <button type="button" name="remove"
                                                                style="margin-bottom: 10px;margin-left: 10px"
                                                                class="btn btn-danger btn_remove_attr">X
                                                        </button>
                                                        <button type="button" name="add" onclick="addAttribute(this)"
                                                                class="btn btn-primary"
                                                                style="margin-bottom: 10px;margin-left: 10px">
                                                            <i class="fa fa-plus"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-7 m-b-xs">
                                    </div>
                                    <div class="col-sm-5">
                                        <div class="col-sm-8 col-sm-offset-5">
                                            <button class="btn btn-white" type="reset">Reset</button>
                                            <button class="btn btn-primary" type="submit">Save changes</button>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <script>
        function addRow() {
            $('.dynamic_field').append('<tr class="attr"><td><input type="text" name="attribute[1][option][]"' +
                ' style="margin-bottom: 10px;" placeholder="Value" class="form-control name_list" /></td>' +
                '<td><button type="button" name="remove" style="margin-bottom: 10px;margin-left: 10px"  class="btn btn-danger btn_remove">X</button>' +
                ' <button type="button" name="add" onclick="addRow()" class="btn btn-primary add" style="margin-bottom: 10px;margin-left: 10px"><i class="fa fa-plus"></i></button></td>' +
                '</tr>');
        }
        $(document).ready(function () {
            $(document).on('click', '.btn_remove', function () {
                $(this).closest('.attr').remove();
            });
        });
        var countAddAtrribute = $(".add-atrribute").length;
        function addAttribute(data) {
            console.log(data.closest('.parentAttr'));
            countAddAtrribute = countAddAtrribute + 1;
//            console.log(.closest('.parentAttr'));
            $('.add-atrribute').append(
                '<div class="parentAttr">' +
                    '<div class="form-group">' +
                        '<label for="Attribute">Attribute (*):</label>' +
                        '<input type="text" name="attribute[' + countAddAtrribute + '][name]" id="Attribute" placeholder="Attribute Name" class="form-control">' +
                    '</div>' +
                    '<div class="form-group">' +
                        '<label for="type">Type (*):</label>' +
                        '<select name="attribute[' + countAddAtrribute + '][type]" id="type" class="form-control">' +
                            '<option value="checkbox">Check Box</option>' +
                            '<option value="radio">Radio</option>' +
                            '<option value="select">Select</option>' +
                            '<option value="text">Text</option>' +
                            '<option value="select multi">Select multi</option>' +
                            '<option value="textarea">Text Area</option>' +
                        '</select>' +
                    '</div>' +
                    '<div class="form-group">' +
                        '<table class="table-light dynamic_fields" id="dynamic_field">' +
                            '<label for="option">Value (*):</label>' +
                                '<tr class="attr">' +
                                    '<td>' +
                                        '<input type="text" name="attribute[' + countAddAtrribute + '][option][]" id="option" style="margin-bottom: 10px;"' +
                                        'placeholder="Value" class="form-control name_list"/>' +
                                    '</td>' +
                                    '<td> ' +
                                        '<button type="button" name="add" onclick="addRowValue($(this).parent().parent().parent())"' +
                                            'class="btn btn-primary add"' +
                                            'style="margin-bottom: 10px;margin-left: 10px">' +
                                            '<i class="fa fa-plus"></i>' +
                                        '</button>' +
                                    '</td>' +
                                '</tr>' +
                        '</table>' +
                    '</div>' +
                    '<div class="form-group text-right">' +
                        '<h3>Add Attribute</h3>' +
                        '<button type="button" name="remove"' +
                            'style="margin-bottom: 10px;margin-left: 10px"' +
                            'class="btn btn-danger btn_remove_attr" onclick="removeAttr($(this).parent().parent())">X' +
                        '</button>' +
                        '<button type="button" name="add" onclick="addAttribute(this)"' +
                            'class="btn btn-primary"' +
                            'style="margin-bottom: 10px;margin-left: 10px">' +
                            '<i class="fa fa-plus"></i>' +
                        '</button>' +
                    '</div>' +
                '</div>'
            );
        }
        function addRowValue(attr) {
            console.log()
            $(attr).append('<tr class="attr"><td><input type="text" name="attribute[' + countAddAtrribute + '][option][]"' +
                ' style="margin-bottom: 10px;" placeholder="Value" class="form-control name_list" /></td>' +
                '<td><button type="button" name="remove" style="margin-bottom: 10px;margin-left: 10px"  class="btn btn-danger btn_removes">X</button>' +
                ' <button type="button" name="add" onclick="addRowValue($(this).parent().parent().parent())" class="btn btn-primary add" style="margin-bottom: 10px;margin-left: 10px"><i class="fa fa-plus"></i></button></td>' +
                '</tr>');
        }
        $(document).ready(function () {
            $(document).on('click', '.btn_removes', function () {
                $(this).closest('.attr').remove();
            });
        });
        function removeAttr(attr) {
            attr.remove();
        }
    </script>
@endsection



