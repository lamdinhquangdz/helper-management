@extends('master.main')
@section('content')
    <div class=" text-center loginscreen animated fadeInDown">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-title">
                        <h5>Create employee account</h5>
                        <a class="btn aa btn-primary " type="button" href="{{route('employee.index')}}"><i class="fa fa-list"></i>&nbsp;List</a>
                    </div>
                    <div class="ibox-content">
                        <form id="form" method="post" action="{{ route('employee.store') }}" class="wizard-big">
                            @csrf
                            <fieldset>
                                <h2>Account Information</h2>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Username *</label>
                                            <input value="{{old('username')}}" id="username" name="username" type="text" class="form-control @error('username') is-invalid @enderror ">
                                            @error('username')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>


                                        <div class="form-group">
                                            <label>Fullname *</label>
                                            <input value="{{old('fullname')}}" id="fullName" name="fullname" type="text" class="form-control @error('fullname') is-invalid @enderror ">
                                            @error('fullname')
                                            <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>


                                        <div class="form-group">
                                            <label>Email *</label>
                                            <input value="{{old('email')}}" id="email" name="email" type="text"  class="form-control @error('email') is-invalid @enderror ">
                                            @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <input value="employee"  name="type" type="hidden" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label>Phone *</label>
                                            <input value="{{ old('phone') }}" id="phone" name="phone" type="text" class="form-control @error('phone') is-invalid @enderror">
                                            @error('phone')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>

                                        <div class="form-group">
                                            <label>Birthday *</label>
                                            <input value="{{old('birthday')}}" id="birthday" name="birthday" type="text" class="form-control @error('birthday') is-invalid @enderror">
                                            @error('birthday')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset>
                                <h2>Profile Information</h2>
                                <div class="row">

                                    <div class="col-lg-12">

                                        <div class="form-group">
                                            <label>Province *</label>
                                            <select class="form-control m-b" id="province" name="province" >
                                                <option></option>
                                                @foreach($province as $provinces)
                                                    <option value="{{$provinces->id}}">{{$provinces->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        @error('province')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror

                                        <div class="form-group">
                                            <label>District *</label>
                                            <select class="form-control m-b" id="district" name="district" >
                                                <option></option>
                                            </select>
                                        </div>
                                        @error('district')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                        <div class="form-group">
                                            <label>Ward *</label>
                                            <select class="form-control m-b" id="ward" name="ward_id" >
                                                <option></option>
                                            </select>
                                        </div>
                                        @error('ward')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                            </fieldset>
                            <div class="form-group row">
                                <div class="col-sm-7 m-b-xs">
                                </div>
                                <div class="col-sm-5">
                                    <div class="col-sm-8 col-sm-offset-5">
                                        <button class="btn btn-white" type="reset">Reset</button>
                                        <button class="btn btn-primary" type="submit">Save changes</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#province').change(function () {
                $('#ward').load(' #ward > * ');
                var province_id = $(this).val()
                $.ajax({
                    url:"{{ route('getDistrict') }}",
                    type:'GET',
                    data: {province_id: province_id},
                    success: function(data) {
                        str = ''
                        data.forEach(district => {
                            str += '<option value=" '+district.id+'">'+district.name+'</option>';

                        });
                        $('#district').html(str)
                    }
                })
            })

            $('#district').change(function () {
                var district_id = $(this).val()
                $.ajax({
                    url:"{{ route('getWards') }}",
                    type:'GET',
                    data: {district_id: district_id},
                    success: function(data) {
                        str = ''
                        data.forEach(ward => {
                            str += '<option value=" '+ward.id+'">'+ward.name+'</option>';

                        });
                        $('#ward').html(str)
                    }
                })
            })
        })
    </script>
@endsection



