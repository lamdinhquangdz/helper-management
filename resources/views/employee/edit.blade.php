@extends('master.main')
@section('content')
    <div class=" text-center loginscreen animated fadeInDown">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-title">
                        <h5>Edit employee account</h5>
                        <a class="btn aa btn-primary " type="button" href="{{route('employee.index')}}"><i class="fa fa-list"></i>&nbsp;List</a>
                    </div>
                    <div class="ibox-content">
                        <form id="form" method="post" action="{{ route('employee.update',['id'=>$user->id]) }}" class="wizard-big">
                            @csrf
                            @method('PUT')
                            <fieldset>
                                <h2>Account Information</h2>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Username *</label>
                                            <input value="{{$user->username}}" id="username" name="username" type="text" class="form-control @error('username') is-invalid @enderror ">
                                            @error('username')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>

                                        <div class="form-group">
                                            <label>Fullname *</label>
                                            <input value="{{$user->fullname}}" id="fullName" name="fullname" type="text" class="form-control @error('fullname') is-invalid @enderror ">
                                            @error('fullname')
                                            <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>

                                        <div class="form-group">
                                            <label>Phone *</label>
                                            <input value="{{$user->phone}}" id="phone" name="phone" type="text" class="form-control @error('phone') is-invalid @enderror">
                                            @error('phone')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <input value="employee"  name="type" type="hidden" class="form-control">
                                        </div>

                                        <div class="form-group">
                                            <label>Birthday *</label>
                                            <input value="{{$user->birthday}}" id="birthday" name="birthday" type="text" class="form-control @error('birthday') is-invalid @enderror">
                                            @error('birthday')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>

                                        <div class="form-group">
                                            <label>Password </label>
                                            <input value="" name="passwords" type="password" class="form-control @error('passwords') is-invalid @enderror">
                                            @error('passwords')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>

                                        <div class="form-group">
                                            <label>Confirm password</label>
                                            <input value="" id="birthday" name="confirm" type="password" class="form-control @error('confirm') is-invalid @enderror">
                                            @error('confirm')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                            <fieldset>
                                <h2>Profile Information</h2>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Province *</label>
                                            <select class="form-control m-b" id="province" name="province" >
                                                <option></option>
                                                @foreach($provinces as $province)
                                                     <option {{$province->id == $user->ward->district->province->id ? 'selected' : ''}} value="{{$province->id}}">{{$province->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        @error('province')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror

                                        <div class="form-group">
                                            <label>District *</label>
                                            <select class="form-control m-b" id="district" name="district" >
                                            </select>
                                        </div>
                                        @error('district')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror

                                        <div class="form-group">
                                            <label>Ward *</label>
                                            <select class="form-control m-b" id="ward" name="ward_id" >
                                            </select>
                                        </div>
                                        @error('ward')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                            </fieldset>
                            <div class="form-group row">
                                <div class="col-sm-7 m-b-xs">
                                </div>
                                <div class="col-sm-5">
                                    <div class="col-sm-8 col-sm-offset-5">
                                        <button class="btn btn-white" type="reset">Reset</button>
                                        <button class="btn btn-primary" type="submit">Save changes</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            var province_id = {{$user->ward->district->province->id}}
            $.ajax({
                url:"{{ route('getDistrict') }}",
                type:'GET',
                data: {province_id: province_id},
                success: function(data) {
                    var district_id = {{$user->ward->district->id}}
                    str = ''
                    data.forEach(district => {
                        if(district.id == district_id){

                             str += '<option value=" '+district.id+'" selected>'+district.name+'</option>';
                        }else{
                            str += '<option value=" '+district.id+'">'+district.name+'</option>';
                        }
                    });
                    $('#district').html(str);

                    $.ajax({
                        url:"{{ route('getWards') }}",
                        type:'GET',
                        data: {district_id: district_id},
                        success: function(data) {
                            var ward_id = {{$user->ward_id}}
                            str = '';
                            data.forEach(ward => {
                                if(ward.id == ward_id){

                                    str += '<option value=" '+ward.id+'" selected>'+ward.name+'</option>';
                                }else{
                                    str += '<option value=" '+ward.id+'">'+ward.name+'</option>';
                                }

                            });
                            $('#ward').html(str)
                        }
                    })
                }
            });


            $('#province').change(function () {
                $('#ward').load(' #ward > * ');
                var province_id = $(this).val()
                $.ajax({
                    url:"{{ route('getDistrict') }}",
                    type:'GET',
                    data: {province_id: province_id},
                    success: function(data) {
                        str = ''
                        data.forEach(district => {
                            str += '<option value=" '+district.id+'">'+district.name+'</option>';

                        });
                        $('#district').html(str)
                    }
                })
            })

            $('#district').change(function () {
                var district_id = $(this).val()
                $.ajax({
                    url:"{{ route('getWards') }}",
                    type:'GET',
                    data: {district_id: district_id},
                    success: function(data) {
                        str = ''
                        data.forEach(ward => {
                            str += '<option value=" '+ward.id+'">'+ward.name+'</option>';

                        });
                        $('#ward').html(str)
                    }
                })
            })
        })
    </script>
@endsection




