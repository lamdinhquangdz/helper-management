@extends('master.main')
@section('content')
    @if (session()->has('success'))
        <div class="alert alert-success">
            @if(is_array(session('success')))
                @foreach (session('success') as $message)
                    {{ $message }}
                @endforeach
            @else
                {{ session('success') }}
            @endif
        </div>
    @endif
    @if (session()->has('error'))
        <div class="alert alert-danger">
            @if(is_array(session('error')))
                @foreach (session('error') as $message)
                    {{ $message }}
                @endforeach
            @else
                {{ session('error') }}
            @endif
        </div>
    @endif
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Employee Management</h5>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-sm-9 m-b-xs">
                            @can('create-employee')
                            <a class="btn btn-primary " type="button" href="{{route('employee.create')}}"><i class="fa fa-plus"></i>&nbsp;Add</a>
                            @endcan
                        </div>
                        <form action="{{route('search.employee')}}" method="get">
                            <div class="col-sm-3">
                                <div class="input-group">
                                    <input type="text" placeholder="Search" name="search" class="input-sm form-control">
                                    <span class="input-group-btn">
                                        <button type="submit" class="btn btn-sm btn-primary"> Go!</button>
                                    </span>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Username</th>
                                <th>Fullname</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Address</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($user as $item)
                                <tr>
                                    <td>{{$item->id}}</td>
                                    <td>{{$item->username}}</td>
                                    <td>{{$item->fullname}}</td>
                                    <td>{{$item->email}}</td>
                                    <td>0{{$item->phone}}</td>
                                    <td>{{$item->ward->name}}</td>
                                    <td>
                                        @can('edit-employee')
                                        <a href="{{route('employee.edit',['id'=>$item->id])}}" class="btn btn-info  dim" type="button">
                                            <i class="fa fa-paste"></i>
                                        </a>
                                        @endcan
                                        @can('delete-employee')
                                        <a href="{{route('employee.destroy',['id'=>$item->id])}}" onclick="return confirm('Có muốn chắc muốn xóa?')" class="btn btn-danger  dim " type="button"><i class="fa fa-times"></i></a>
                                            @endcan
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-sm-5 m-b-xs">
                        </div>
                        <div class="col-sm-7">
                            <div class="input-group">
                                {{$user->links()}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection

