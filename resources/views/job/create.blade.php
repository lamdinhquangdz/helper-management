@extends('master.main')
@section('content')
    <div class=" text-center loginscreen animated fadeInDown">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-title ">
                        <h5>Create job</h5>
                        <a class="btn aa btn-primary " type="button" href="{{route('job.index')}}"><i
                                class="fa fa-list"></i>&nbsp;List</a>
                    </div>
                    <div class="ibox-content">
                        <form id="form" method="post" action="{{ route('job.store') }}" class="wizard-big">
                            @csrf
                            <fieldset>
                                <h2>Job Information</h2>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Title </label>
                                            <input value="{{old('title')}}" id="title" name="title" type="text"
                                                   class="form-control @error('title') is-invalid @enderror ">
                                            @error('title')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>

                                        <div class="form-group">
                                            <label>Category</label>
                                            <select class="form-control m-b" id="category" name="category_id">
                                                <option>--- Select category ---</option>
                                                @foreach($category as $item)
                                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        @error('category_id')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror

                                        <div class="form-group" id="attribute">

                                        </div>

                                        <div class="form-group">
                                            <label>Detail</label>
                                            <textarea name="detail" id="" cols="30" rows="10"></textarea>
                                        </div>

                                    </div>

                                    <div class="col-lg-12">

                                        <div class="form-group">
                                            <label>Province</label>
                                            <select class="form-control m-b" id="province" name="province">
                                                <option></option>
                                                @foreach($province as $provinces)
                                                    <option value="{{$provinces->id}}">{{$provinces->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        @error('province')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror

                                        <div class="form-group">
                                            <label>District</label>
                                            <select id="district" class="form-control m-b" name="district">
                                                <option></option>
                                            </select>
                                        </div>
                                        @error('district')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror

                                        <div class="form-group">
                                            <label>Ward</label>
                                            <select class="form-control m-b" id="ward" name="ward_id">
                                                <option></option>
                                            </select>
                                        </div>
                                        @error('ward_id')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror

                                        <div class="form-group">
                                            <label>Home number </label>
                                            <input value="{{old('home_number')}}" id="home_number" name="home_number"
                                                   type="text"
                                                   class="form-control @error('home_number') is-invalid @enderror ">
                                            @error('home_number')
                                            <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <input value="0" name="status" type="hidden" class="form-control">
                                    </div>
                                </div>
                            </fieldset>
                            <div class="form-group row">
                                <div class="col-sm-7 m-b-xs">
                                </div>
                                <div class="col-sm-5">
                                    <div class="col-sm-8 col-sm-offset-5">
                                        <button class="btn btn-white" type="reset">Reset</button>
                                        <button class="btn btn-primary" type="submit">Save changes</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <script src="//cdn.ckeditor.com/4.15.1/standard/ckeditor.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#province').change(function () {
                $('#ward').load(' #ward > * ');
                var province_id = $(this).val()
                $.ajax({
                    url: "{{ route('getDistrict') }}",
                    type: 'GET',
                    data: {province_id: province_id},
                    success: function (data) {
                        str = ''
                        data.forEach(district => {
                            str += '<option value=" ' + district.id + '">' + district.name + '</option>';

                        });
                        $('#district').html(str)
                    }
                })
            })

            $('#district').change(function () {
                var district_id = $(this).val()
                $.ajax({
                    url: "{{ route('getWards') }}",
                    type: 'GET',
                    data: {district_id: district_id},
                    success: function (data) {
                        str = ''
                        data.forEach(ward => {
                            str += '<option value=" ' + ward.id + '">' + ward.name + '</option>';

                        });
                        $('#ward').html(str)
                    }
                })
            })

            $('#category').change(function () {

                var category_id = $(this).val()
                $.ajax({
                    url: "{{route('getAttribute')}}",
                    type: 'GET',
                    data: {category_id: category_id},
                    success: function(data){
                        attr = ''
                        console.log(data.attributes)
                        data.attributes.forEach(function (attribute, index) {
                            if (attribute.type == 'checkbox' || attribute.type == 'radio' || attribute.type == 'text') {
                                attr += '<input type="hidden" value="' + attribute.id + '"> <label>' + attribute.name + '</label>';
                                attr += '<div>'
                                if (attribute.type == 'checkbox' || attribute.type == 'radio') {
                                    attribute.option.forEach(option => {
                                        attr += '<input type="' + attribute.type + '" name="attribute[' + attribute.id + '][]" value="' + option + '"> <span>' + option + '</span> <br>';
                                    });
                                } else {
                                    attr += '<input class="form-control" type="' + attribute.type + '" name="attribute[' + attribute.id + '][]" value="">';
                                }
                                attr += '</div>';
                            } else if (attribute.type == 'select') {
                                attr += '<input type="hidden" value="' + attribute.id + '"> <label>' + attribute.name + '</label>';
                                attr += '<div>'
                                attr += '<select class="form-control m-b">'
                                attribute.option.forEach(option => {
                                    attr += '<option type="' + attribute.type + '" name="attribute[' + attribute.id + '][]" value="' + option + '"> <span>' + option + '</span> ';
                                    attr += '</option> <br>'
                                });
                                attr += '</select>'
                            } else if (attribute.type == 'textarea') {
                                attr += '<div class="form-group">'
                                attr += '<input type="hidden" value="' + attribute.id + '"> <label style="display: block;">' + attribute.name + '</label>'
                                attr += '<textarea name="attribute[' + attribute.id + '][]" id="text2" cols="138" rows="10"></textarea>'
                                attr += '</div>'
                            } else if (attribute.type == 'select multi') {
                                attr += '<input type="hidden" value="' + attribute.id + '"> <label>' + attribute.name + '</label>';
                                attr += '<div>'
                                attr += '<select class="form-control m-b" multiple>'
                                attribute.option.forEach(option => {
                                    attr += '<option type="' + attribute.type + '" name="attribute[' + attribute.id + '][]" value="' + option + '"> <span>' + option + '</span> ';
                                    attr += '</option> <br>'
                                });
                                attr += '</select>'
                            }
                            if (index != data.length - 1) {
                                attr += '<hr>';
                            }

                        });
                        $('#attribute').html(attr);
                    }
                })
            })
        })
    </script>
    <script src="{{asset('backend/js/plugins/summernote/summernote.min.js')}}"></script>
    <script>
        $(document).ready(function () {
            CKEDITOR.replace('detail')
        });
    </script>
@endsection

