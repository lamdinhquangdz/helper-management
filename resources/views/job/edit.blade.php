@extends('master.main')
@section('content')
    <div class=" text-center loginscreen animated fadeInDown">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-title ">
                        <h5>Edit job</h5>
                        <a class="btn aa btn-primary " type="button" href="{{route('job.index')}}"><i class="fa fa-list"></i>&nbsp;List</a>
                    </div>
                    <div class="ibox-content">
                        <form id="form" method="post" action="{{ route('job.update',['id'=>$job->id]) }}" class="wizard-big">
                            @csrf
                            <fieldset>
                                <h2>Job Information </h2>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Title </label>
                                            <input value="{{$job->title}}" id="title" name="title" type="text" class="form-control @error('title') is-invalid @enderror ">
                                            @error('title')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>

                                        <div class="form-group">
                                            <label>Category</label>
                                            <select class="form-control m-b" id="category" name="category_id" >
                                                <option>--- Select category ---</option>
                                                @foreach($category as $item)
                                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        @error('category_id')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror

                                        <div class="form-group" id="attribute">
                                        </div>

                                        <div class="form-group">
                                            <label>Detail</label>
                                            <textarea name="detail" class="detail" id="" cols="30" rows="10">{{$job->detail}}</textarea>
                                        </div>

                                    </div>

                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Province *</label>
                                            <select class="form-control m-b" id="province" name="province" >
                                                <option></option>
                                                @foreach($provinces as $province)
                                                    <option {{$province->id == $job->ward->district->province->id ? 'selected' : ''}} value="{{$province->id}}">{{$province->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        @error('province')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror

                                        <div class="form-group">
                                            <label>District *</label>
                                            <select class="form-control m-b" id="district" name="district" >
                                            </select>
                                        </div>
                                        @error('district')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror

                                        <div class="form-group">
                                            <label>Ward *</label>
                                            <select class="form-control m-b" id="ward" name="ward_id" >
                                            </select>
                                        </div>
                                        @error('ward')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror

                                        <div class="form-group">
                                            <label>Home number </label>
                                            <input value="{{$job->home_number}}" id="home_number" name="home_number" type="text" class="form-control @error('home_number') is-invalid @enderror ">
                                            @error('home_number')
                                            <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                            <div class="form-group row">
                                <div class="col-sm-7 m-b-xs">
                                </div>
                                <div class="col-sm-5">
                                    <div class="col-sm-8 col-sm-offset-5">
                                        <button class="btn btn-white" type="reset">Reset</button>
                                        <button class="btn btn-primary" type="submit">Save changes</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <script src="//cdn.ckeditor.com/4.15.1/standard/ckeditor.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            var province_id = {{$job->ward->district->province->id}}
            $.ajax({
                url:"{{ route('getDistrict') }}",
                type:'GET',
                data: {province_id: province_id},
                success: function(data) {
                    var district_id = {{$job->ward->district->id}}
                        str = ''
                    data.forEach(district => {
                        if(district.id == district_id){

                            str += '<option value=" '+district.id+'" selected>'+district.name+'</option>';
                        }else{
                            str += '<option value=" '+district.id+'">'+district.name+'</option>';
                        }
                    });

                    $('#district').html(str);
                    $.ajax({
                        url:"{{ route('getWards') }}",
                        type:'GET',
                        data: {district_id: district_id},
                        success: function(data) {
                            var ward_id = {{$job->ward_id}}
                                str = '';
                            data.forEach(ward => {
                                if(ward.id == ward_id){

                                    str += '<option value=" '+ward.id+'" selected>'+ward.name+'</option>';
                                }else{
                                    str += '<option value=" '+ward.id+'">'+ward.name+'</option>';
                                }

                            });
                            $('#ward').html(str)
                        }
                    })
                }
            });

            $('#province').change(function () {
                $('#ward').load(' #ward > * ');
                var province_id = $(this).val()
                $.ajax({
                    url:"{{ route('getDistrict') }}",
                    type:'GET',
                    data: {province_id: province_id},
                    success: function(data) {
                        str = ''
                        data.forEach(district => {
                            str += '<option value=" '+district.id+'">'+district.name+'</option>';

                        });
                        $('#district').html(str)
                    }
                })
            })

            $('#district').change(function () {
                var district_id = $(this).val()
                $.ajax({
                    url:"{{ route('getWards') }}",
                    type:'GET',
                    data: {district_id: district_id},
                    success: function(data) {
                        str = ''
                        data.forEach(ward => {
                            str += '<option value=" '+ward.id+'">'+ward.name+'</option>';

                        });
                        $('#ward').html(str)
                    }
                })
            })

            $('#category').change(function (){

                var category_id = $(this).val();
                $.ajax({
                    url:"{{route('getJobAttribute')}}",
                    type:'GET',
                    data:{
                        category_id:category_id,
                        job_id: {{$job->id}}
                    },
                    success: function(data){
                        var check = [];
                        data.attributeValues.forEach(function (attValue){
                            JSON.parse(attValue.value).forEach(function (item){
                                check.push(item);
                            })
                        })
                        attr = ''
                        data.attributes.forEach( function(attribute, index){
                            if(attribute.type == 'checkbox'||attribute.type=='radio'||attribute.type=='text'){
                                attr += '<input type="hidden" value="'+attribute.id+'"> <label>'+attribute.name+'</label>';
                                attr += '<div>'
                                if (attribute.type == 'checkbox'||attribute.type=='radio') {
                                    attribute.option.forEach(option => {
                                        if(check.indexOf(option) != -1){
                                            attr += '<input type="' + attribute.type + '" name="attribute[' + attribute.id + '][]" value="' + option + '" checked> <span>' + option + '</span> <br>';
                                        }else{
                                            attr += '<input type="' + attribute.type + '" name="attribute[' + attribute.id + '][]" value="' + option + '"> <span>' + option + '</span> <br>';
                                        }
                                    });
                                }else {
                                    attr += '<input class="form-control" type="' + attribute.type + '" name="attribute[' + attribute.id + '][]" @foreach($attJob as $item) @foreach($attribute as $type) @if($item->attribute_id == $type->id && $type->type == 'text' && $item->job_id==$job->id) @foreach($item->value as $show) value="{{$show}}" @endforeach @endif @endforeach @endforeach>';
                                }
                                attr += '</div>';
                            }
                            else if(attribute.type=='select') {
                                attr += '<input type="hidden" value="'+attribute.id+'"> <label>'+attribute.name+'</label>';
                                attr += '<div>'
                                attr +='<select class="form-control m-b" name="attribute['+attribute.id+'][]">'
                                attr += '<option value=""> <span>' +  '</span> </option>'
                                attribute.option.forEach(option =>{
                                    if(check.indexOf(option) != -1){
                                        attr += '<option type="'+attribute.type+'" selected  value="'+option+'"> <span>'+option+'</span> ';
                                        attr +='</option> <br>'
                                    }else {
                                        attr += '<option type="' + attribute.type + '"  value="' + option + '"> <span>' + option + '</span> ';
                                        attr += '</option> <br>'
                                    }
                                });
                                attr +='</select>'
                            }
                            else if (attribute.type=='textarea'){
                                attr += '<div class="form-group">'
                                attr += '<input type="hidden" value="'+attribute.id+'"> <label style="display: block;">'+attribute.name+'</label>'
                                attr += '<textarea name="attribute[' + attribute.id + '][]" class="form-control" id="text2" cols="138" rows="10">'
                                @foreach($attJob as $item)
                                   @foreach($attribute as $type)
                                       @if($item->attribute_id == $type->id && $type->type == 'textarea'&& $item->job_id==$job->id)
                                           @foreach($item->value as $show)
                                attr += '{{$show}}'
                                           @endforeach
                                        @endif
                                   @endforeach
                                @endforeach
                                attr += '</textarea>'
                                attr += '</div>'
                            }
                            else if(attribute.type=='select multi'){
                                attr += '<input type="hidden" value="'+attribute.id+'"> <label>'+attribute.name+'</label>';
                                attr += '<div>'
                                attr +='<select class="form-control m-b" multiple name="attribute[' + attribute.id + '][]">'
                                attr += '<option value=""> <span>' +  '</span> </option>'
                                attribute.option.forEach(option =>{
                                    if(check.indexOf(option) != -1) {
                                        attr += '<option type="' + attribute.type + '" selected value="' + option + '"> <span>' + option + '</span> ';
                                        attr += '</option> <br>'
                                    }else {
                                        attr += '<option type="' + attribute.type + '" value="' + option + '"> <span>' + option + '</span> ';
                                        attr += '</option> <br>'
                                    }
                                });
                                attr +='</select>'
                            }
                            if(index != data.length-1){
                                attr += '<hr>';
                            }

                        });
                        $('#attribute').html(attr);
                    }
                })
            })
        })
    </script>
    <script src="{{asset('backend/js/plugins/summernote/summernote.min.js')}}"></script>
    <script>
        $(document).ready(function() {
            CKEDITOR.replace('detail')

            var categoryId = {{$job->category_id}}
            $('#category option[value="' + categoryId + '"]').attr('selected', 'selected').trigger('change');

        })
    </script>
@endsection


