
@extends('master.main')
@section('content')
    @if (session()->has('success'))
        <div class="alert alert-success">
            @if(is_array(session('success')))
                @foreach (session('success') as $message)
                    {{ $message }}
                @endforeach
            @else
                {{ session('success') }}
            @endif
        </div>
    @endif
    @if (session()->has('error'))
        <div class="alert alert-danger">
            @if(is_array(session('error')))
                @foreach (session('error') as $message)
                    {{ $message }}
                @endforeach
            @else
                {{ session('error') }}
            @endif
        </div>
    @endif
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Job Management</h5>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-sm-6 m-b-xs">
                            @can('create-job')
                                <a class="btn btn-primary " type="button" href="{{route('job.create')}}"><i class="fa fa-plus"></i>&nbsp;Add</a>
                            @endcan
                        </div>
                        <form action="" method="get">
                            <div class="col-sm-6 row">
                                <div class="col-sm-4">
                                    <div class=" form-group input-groupphp">
                                        <select name="status" class="form-control input-sm">
                                            <option value="">Step</option>
                                            <option value="pendding" @if(Request::get('status') == 'pendding') selected @endif>pendding</option>
                                            <option value="assigned" @if(Request::get('status') == 'assigned') selected @endif>assigned</option>
                                            <option value="accept" @if(Request::get('status') == 'accept') selected @endif>accept</option>
                                            <option value="confirm" @if(Request::get('status') == 'confirm') selected @endif>confirm</option>
                                            <option value="doing" @if(Request::get('status') == 'doing') selected @endif>doing</option>
                                            <option value="complete" @if(Request::get('status') == 'complete') selected @endif>complete</option>
                                            <option value="customer accept" @if(Request::get('status') == 'customer accept') selected @endif>customer accept</option>
                                            <option value="unfinished" @if(Request::get('status') == 'unfinished') selected @endif>unfinished</option>
                                            <option value="done" @if(Request::get('status') == 'done') selected @endif>done</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    <div class=" form-group input-group">
                                        <input type="text" placeholder="Search" name="search" class="input-sm form-control">
                                        <span class="input-group-btn">
                                        <button type="submit" class="btn btn-sm btn-primary"> Go!</button>
                                    </span>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Title</th>
                                <th>Detail</th>
                                <th>Username</th>
                                <th>Address</th>
                                <th>Employee</th>
                                <th>Step</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($job as $item)
                                <tr>
                                    <td>{{$item->id}}</td>
                                    <td>{{$item->title}}</td>
                                    <td>{!! Str::limit($item->detail,30) !!}</td>
                                    <td>{{$item->user->username}}</td>
                                    <td>{{$item->ward->name}}</td>
                                    <td>
                                        @foreach($user as $users)
                                                @if($users->id==$item->employee_id)
                                                    {{$users->fullname}}
                                                @endif
                                        @endforeach
                                    </td>
                                    <td>
                                        @if($item->status == 'pendding')
                                            <span class="label">{{$item->status}}</span>
                                        @elseif($item->status == 'assigned')
                                            <span class="label label-warning">{{$item->status}}</span>
                                        @elseif($item->status == 'accept')
                                            <span class="label label-info">{{$item->status}}</span>
                                        @elseif($item->status == 'confirm')
                                            <span class="label label-info">{{$item->status}}</span>
                                        @elseif($item->status == 'doing')
                                            <span class="label label-primary">{{$item->status}}</span>
                                        @elseif($item->status == 'complete')
                                            <span class="label label-primary">{{$item->status}}</span>
                                        @elseif($item->status == 'unfinished')
                                            <span class="label label-warning-light">{{$item->status}}</span>
                                        @elseif($item->status == 'customer accept')
                                            <span class="label">{{$item->status}}</span>
                                        @else
                                            <span class="label label-success">{{$item->status}}</span>
                                        @endif
                                    </td>
                                    <td class="align">

                                            <a href="{{route('job.show',['id'=>$item->id])}}" class="btn btn-primary  dim" type="button">
                                                <i class="fa fa-eye"></i>
                                            </a>

                                        @can('edit-job')
                                                @if(in_array($item->status,['pendding','assigned']))
                                                    <a href="{{route('job.edit',['id'=>$item->id])}}" class="btn btn-info  dim" type="button">
                                                        <i class="fa fa-paste"></i>
                                                    </a>
                                                @endif
                                            @endcan

                                        @can('delete-job')
                                            @if(in_array($item->status,['pendding','assigned','confirm','accept','done']))
                                                <a href="{{route('job.destroy',['id'=>$item->id])}}" onclick="return confirm('Có muốn chắc muốn xóa?')" class="btn btn-danger  dim " type="button"><i class="fa fa-times"></i></a>
                                            @endif
                                        @endcan
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-sm-5 m-b-xs">
                        </div>
                        <div class="col-sm-7">
                            <div class="input-group">
                                {{$job->links()}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
