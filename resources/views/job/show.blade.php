@extends('master.main')
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Step & history</h5>
                    <a class="btn aa btn-primary " type="button" href="{{route('job.index')}}"><i class="fa fa-list"></i>&nbsp;List</a>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-sm-9 m-b-xs">
                        </div>

                        <div class="col-sm-3">
                            <div class="input-group">
                            </div>
                        </div>

                    </div>
                    <form action="{{route('job.history',['id'=>$job->id])}}" method="post">
                        @csrf
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <tr>
                                    <th>Title</th>
                                    <td>{{$job->title}}</td>
                                </tr>
                                <tr>
                                    <th>Detail</th>
                                    <td>{!!$job->detail!!}</td>
                                </tr>
                                <tr>
                                    <th>Username</th>
                                    <td>{{$job->user->username}}</td>
                                </tr>
                                <tr>
                                    <th>Address</th>
                                    <td>{{$job->home_number}}</td>
                                </tr>
                                <tr>
                                    <th>Employee</th>
                                    <td>
                                        @if($job->employee_id)
                                            @foreach($user as $users)
                                                @if($users->id==$job->employee_id)
                                                    {{$users->fullname}}
                                                @endif
                                            @endforeach
                                        @else
                                        Nobody
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <th>Status</th>
                                    <td>{{$job->status}}</td>
                                </tr>
                            </table>
                            @can('view-admin')
                                @if(!in_array($job->status,['done','complete','customer accept','unfinished','doing']))
                                    <div class="form-group">
                                        <label>Employee</label>
                                        <select class="form-control m-b" name="employee_id">
                                            <option></option>
                                            @foreach($user as $item)
                                                <option
                                                    {{$item->id == $job->employee_id ? 'selected' : ''}} value="{{$item->id}}">{{$item->fullname}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                @endif
                            @endcan

                            @if($job->status!='done')
                            <div class="form-group" >
                                <label>Status</label>
                                <select class="form-control m-b" name="status">
                                    @if(!in_array($job->status,['done','complete','customer accept','doing','unfinished']))
                                        @if($job->status == 'pendding')
                                            <option selected value="pendding">pendding</option>
                                        @else
                                            <option value="pendding">pendding</option>
                                        @endif
                                    @endif
                                    @if(auth()->user()->type == ADMIN)
                                        @if(!in_array($job->status,['done','complete','customer accept','doing','unfinished']))
                                            @if($job->status == 'assigned')
                                                <option selected value="assigned">assigned</option>
                                            @else
                                                <option value="assigned">assigned</option>
                                            @endif
                                        @endif
                                    @endif

                                    @if(auth()->user()->type == EMPLOYEE)
                                        @if($job->status == 'assigned')
                                            @if($job->status == 'accept')
                                                <option selected value="accept">accept</option>
                                            @else
                                                <option value="accept">accept</option>
                                            @endif
                                        @endif
                                    @endif

                                    @if(auth()->user()->id==$job->user_id)
                                        @if($job->status == 'accept')
                                            @if($job->status == 'confirm')
                                                <option selected value="confirm">confirm</option>
                                            @else
                                                <option value="confirm">confirm</option>
                                            @endif
                                        @endif
                                    @endif

                                    @if(auth()->user()->type == EMPLOYEE)
                                        @if(in_array($job->status,['confirm','complete','unfinished']))
                                            @if($job->status == 'doing')
                                                <option selected value="doing">doing</option>
                                            @else
                                                <option value="doing">doing</option>
                                            @endif
                                        @endif
                                    @endif

                                    @if(auth()->user()->type == EMPLOYEE)
                                        @if($job->status=='doing')
                                            @if($job->status == 'complete')
                                                <option selected value="complete">complete</option>
                                            @else
                                                <option value="complete">complete</option>
                                            @endif
                                        @endif
                                    @endif

                                    @if(auth()->user()->id==$job->user_id)
                                        @if($job->status == 'complete')
                                            @if($job->status == 'customer accept')
                                                <option selected value="customer accept">customer accept</option>
                                            @else
                                                <option value="customer accept">customer accept</option>
                                            @endif

                                                @if($job->status == 'unfinished')
                                                    <option selected value="unfinished">unfinished</option>
                                                @else
                                                    <option value="unfinished">unfinished</option>
                                                @endif
                                        @endif
                                    @endif

                                    @if(auth()->user()->type == ADMIN)
                                        @if($job->status=='customer accept')
                                            @if($job->status == 'done')
                                                <option selected value="done">done</option>
                                            @else
                                                <option value="done">done</option>
                                            @endif
                                        @endif
                                    @endif

                                </select>
                            </div>
                            @endif
                        </div>
                        <label for="">History</label>
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thread>
                                    <tr>
                                        <th>People add step</th>
                                        <th></th>
                                        <th>Step</th>
                                    </tr>
                                </thread>
                                <tbody>
                                @foreach($check as $item)
                                    <tr>
                                        <td>{{$item->user->username}}</td>
                                        <td>
                                            has moved to step
                                        </td>
                                        <td>
                                            @if($item->status == 'pendding')
                                                <span class="label">{{$item->status}}</span>
                                            @elseif($item->status == 'assigned')
                                                <span class="label label-warning">{{$item->status}}</span>
                                            @elseif($item->status == 'accept')
                                                <span class="label label-info">{{$item->status}}</span>
                                            @elseif($item->status == 'confirm')
                                                <span class="label label-info">{{$item->status}}</span>
                                            @elseif($item->status == 'doing')
                                                <span class="label label-primary">{{$item->status}}</span>
                                            @elseif($item->status == 'complete')
                                                <span class="label label-primary">{{$item->status}}</span>
                                            @elseif($item->status == 'unfinished')
                                                <span class="label label-warning-light">{{$item->status}}</span>
                                            @elseif($item->status == 'customer accept')
                                                <span class="label label-primary">{{$item->status}}</span>
                                            @else
                                                <span class="label label-success">{{$item->status}}</span>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-7 m-b-xs">
                            </div>
                            <div class="col-sm-5">
                                <div class="col-sm-8 col-sm-offset-5">
                                    @if($job->status!='done')
                                        <button class="btn btn-white" type="reset">Reset</button>
                                        <button class="btn btn-primary" type="submit">Save changes</button>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
@endsection
