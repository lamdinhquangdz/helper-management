<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element"> <span>
                            <img alt="image" class="img-circle" src="{{asset('backend/img/profile_small.jpg')}}" />
                             </span>
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear">
                                <span class="block m-t-xs">
                                    @if(Auth::check())
                                    <strong style="color: whitesmoke" class="font-bold">{{ Auth::user()->fullname }}</strong>
                                    @else
                                        <strong style="color: whitesmoke" class="font-bold">Guest</strong>
                                    @endif
                                </span>
                                <span class="text-muted text-xs block">
                                    {{ Auth::user()->type }} <b class="caret"></b>
                                </span>
                            </span>
                    </a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        <li><a href="profile.html">Profile</a></li>
                        <li><a href="contacts.html">Contacts</a></li>
                        <li><a href="mailbox.html">Mailbox</a></li>
                        <li class="divider"></li>
                        <li><a href="login.html">Logout</a></li>
                    </ul>
                </div>
                <div class="logo-element">
                    IN+
                </div>
            </li>
            @can('view-admin','view-role','view-permission')
            <li class="@if(Route::is('admin.index')||Route::is('role.index')) active @endif">
                <a href=""><i class="fa fa-table"></i> <span class="nav-label">Admin</span> <span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    @can('view-admin')
                    <li class="@if(Route::is('user.index')) active @endif">
                        <a href="{{route('user.index')}}">User</a>
                    </li>
                    @endcan
                    @can('view-role')
                    <li class="@if(Route::is('role.index')) active @endif">
                        <a href="{{route('role.index')}}">Role</a>
                    </li>
                    @endcan
                </ul>
            </li>
            @endcan
            @can('view-customer')
            <li class="@if(Route::is('customer.index')) active @endif">
                <a href="{{route('customer.index')}}"><i class="fa fa-shopping-cart"></i> <span class="nav-label">Customer</span></a>
            </li>
            @endcan
            @can('view-employee')
            <li class="@if(Route::is('employee.index')) active @endif">
                <a href="{{route('employee.index')}}"><i class="fa fa-sitemap"></i> <span class="nav-label">Employee </span></a>
            </li>
            @endcan
            @can('view-job','view-category','view-attribute')
            <li class="@if(Route::is('category.index')||Route::is('attribute.index')||Route::is('job.index')) active @endif">
                <a href=""><i class="fa fa-database"></i> <span class="nav-label">Job Management</span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse ">
                    @can('view-category')
                        <li class="@if(Route::is('category.index')) active @endif">
                            <a href="{{route('category.index')}}">Category</a>
                        </li>
                    @endcan
                    @can('view-attribute')
                        <li class="@if(Route::is('attribute.index')) active @endif">
                            <a href="{{route('attribute.index')}}">Attribute</a>
                        </li>
                    @endcan
                    @can('view-job')
                    <li class="@if(Route::is('job.index')) active @endif">
                        <a href="{{route('job.index')}}">Job</a>
                    </li>
                    @endcan
                </ul>
            </li>
            @endcan
        </ul>
    </div>
</nav>
