@extends('master.main')
@section('content')
    <div class=" text-center loginscreen animated fadeInDown">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-title">
                        <h5>Create permission</h5>
                    </div>
                    <div class="ibox-content">
                        <form id="form" method="post" action="{{ route('permission.store') }}" class="wizard-big">
                            @csrf
                            <fieldset>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Name</label>
                                            <input value="{{old('name')}}" id="name" name="name" type="text" class="form-control @error('name') is-invalid @enderror required">
                                        </div>
                                        @error('name')
                                        <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-7 m-b-xs">
                                    </div>
                                    <div class="col-sm-5">
                                        <div class="col-sm-8 col-sm-offset-5">
                                            <button class="btn btn-white" type="reset">Reset</button>
                                            <button class="btn btn-primary" type="submit">Save changes</button>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection


