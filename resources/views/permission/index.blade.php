@extends('master.main')
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Permission </h5>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-sm-9 m-b-xs">
                        </div>
                        <form action="{{route('search.permission')}}" method="get">
                            <div class="col-sm-3">
                                <div class="input-group">
                                    <input type="text" placeholder="Search" name="search" class="input-sm form-control">
                                    <span class="input-group-btn">
                                        <button type="submit" class="btn btn-sm btn-primary"> Go!</button>
                                    </span>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Permission Name </th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($permission as $item)
                                    <tr>
                                        <td>{{$item->id}}</td>
                                        <td>{{$item->name}}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-sm-5 m-b-xs">
                        </div>
                        <div class="col-sm-7">
                            <div class="input-group">
                                {{$permission->links()}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection


