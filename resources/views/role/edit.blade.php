@extends('master.main')
@section('content')
    <div class=" text-center loginscreen animated fadeInDown">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-title">
                        <h5>Create account</h5>
                        <a class="btn aa btn-primary " type="button" href="{{route('role.index')}}"><i class="fa fa-list"></i>&nbsp;List</a>
                    </div>
                    <div class="ibox-content">
                        <form id="form" method="post" action="{{ route('role.update',['id'=>$role->id]) }}" class="wizard-big">
                            @csrf
                            <fieldset>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Name *</label>
                                            <input value="{{$role->name}}" id="name" name="name" type="text" class="form-control @error('name') is-invalid @enderror required">
                                        </div>
                                        @error('name')
                                        <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                        @enderror
                                    </div>

                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label>Permission *</label>
                                            <div>
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        Admin
                                                    </div>
                                                    <div class="panel-body">
                                                        @foreach($permission as $item)
                                                            @php
                                                                $check = 0;
                                                            @endphp
                                                            @foreach ($role->permissions as $ite)
                                                                @if ($item->id == $ite->id)
                                                                    @php
                                                                        $check = 1;
                                                                    @endphp
                                                                @endif
                                                            @endforeach
                                                            @if($item->key == 'admin')
                                                                @if($check==1)
                                                                    <div class="col-lg-3">
                                                                        <input checked type="checkbox" name="permission[]" value="{{$item->id}}"> {{$item->name}}
                                                                    </div>
                                                                @else
                                                                    <div class="col-lg-3">
                                                                        <input type="checkbox" name="permission[]" value="{{$item->id}}"> {{$item->name}}
                                                                    </div>
                                                                @endif
                                                            @endif
                                                        @endforeach
                                                    </div>
                                                </div>
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        Customer
                                                    </div>
                                                    <div class="panel-body">
                                                        @foreach($permission as $item)
                                                            @php
                                                                $check = 0;
                                                            @endphp
                                                            @foreach ($role->permissions as $ite)
                                                                @if ($item->id == $ite->id)
                                                                    @php
                                                                        $check = 1;
                                                                    @endphp
                                                                @endif
                                                            @endforeach
                                                            @if($item->key == 'customer')
                                                                @if($check==1)
                                                                    <div class="col-lg-3">
                                                                        <input checked type="checkbox" name="permission[]" value="{{$item->id}}"> {{$item->name}}
                                                                    </div>
                                                                @else
                                                                    <div class="col-lg-3">
                                                                        <input type="checkbox" name="permission[]" value="{{$item->id}}"> {{$item->name}}
                                                                    </div>
                                                                @endif
                                                            @endif
                                                        @endforeach
                                                    </div>
                                                </div>

                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        Employee
                                                    </div>
                                                    <div class="panel-body">
                                                        @foreach($permission as $item)
                                                            @php
                                                                $check = 0;
                                                            @endphp
                                                            @foreach ($role->permissions as $ite)
                                                                @if ($item->id == $ite->id)
                                                                    @php
                                                                        $check = 1;
                                                                    @endphp
                                                                @endif
                                                            @endforeach
                                                            @if($item->key == 'employee')
                                                                @if($check==1)
                                                                    <div class="col-lg-3">
                                                                        <input checked type="checkbox" name="permission[]" value="{{$item->id}}"> {{$item->name}}
                                                                    </div>
                                                                @else
                                                                    <div class="col-lg-3">
                                                                        <input type="checkbox" name="permission[]" value="{{$item->id}}"> {{$item->name}}
                                                                    </div>
                                                                @endif
                                                            @endif
                                                        @endforeach
                                                    </div>
                                                </div>

                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        Role
                                                    </div>
                                                    <div class="panel-body">
                                                        @foreach($permission as $item)
                                                            @php
                                                                $check = 0;
                                                            @endphp
                                                            @foreach ($role->permissions as $ite)
                                                                @if ($item->id == $ite->id)
                                                                    @php
                                                                        $check = 1;
                                                                    @endphp
                                                                @endif
                                                            @endforeach
                                                            @if($item->key == 'role')
                                                                @if($check==1)
                                                                    <div class="col-lg-3">
                                                                        <input checked type="checkbox" name="permission[]" value="{{$item->id}}"> {{$item->name}}
                                                                    </div>
                                                                @else
                                                                    <div class="col-lg-3">
                                                                        <input type="checkbox" name="permission[]" value="{{$item->id}}"> {{$item->name}}
                                                                    </div>
                                                                @endif
                                                            @endif
                                                        @endforeach
                                                    </div>
                                                </div>

{{--                                                <div class="panel panel-default">--}}
{{--                                                    <div class="panel-heading">--}}
{{--                                                        Permission--}}
{{--                                                    </div>--}}
{{--                                                    <div class="panel-body">--}}
{{--                                                        @foreach($permission as $item)--}}
{{--                                                            @php--}}
{{--                                                                $check = 0;--}}
{{--                                                            @endphp--}}
{{--                                                            @foreach ($role->permissions as $ite)--}}
{{--                                                                @if ($item->id == $ite->id)--}}
{{--                                                                    @php--}}
{{--                                                                        $check = 1;--}}
{{--                                                                    @endphp--}}
{{--                                                                @endif--}}
{{--                                                            @endforeach--}}
{{--                                                            @if($item->key == 'permission')--}}
{{--                                                                @if($check==1)--}}
{{--                                                                    <div class="col-lg-3">--}}
{{--                                                                        <input checked type="checkbox" name="permission[]" value="{{$item->id}}"> {{$item->name}}--}}
{{--                                                                    </div>--}}
{{--                                                                @else--}}
{{--                                                                <div class="col-lg-3">--}}
{{--                                                                    <input type="checkbox" name="permission[]" value="{{$item->id}}"> {{$item->name}}--}}
{{--                                                                </div>--}}
{{--                                                                @endif--}}
{{--                                                            @endif--}}
{{--                                                        @endforeach--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}

                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        Category
                                                    </div>
                                                    <div class="panel-body">
                                                        @foreach($permission as $item)
                                                            @php
                                                                $check = 0;
                                                            @endphp
                                                            @foreach ($role->permissions as $ite)
                                                                @if ($item->id == $ite->id)
                                                                    @php
                                                                        $check = 1;
                                                                    @endphp
                                                                @endif
                                                            @endforeach
                                                            @if($item->key == 'category')
                                                                @if($check==1)
                                                                    <div class="col-lg-3">
                                                                        <input checked type="checkbox" name="permission[]" value="{{$item->id}}"> {{$item->name}}
                                                                    </div>
                                                                @else
                                                                    <div class="col-lg-3">
                                                                        <input type="checkbox" name="permission[]" value="{{$item->id}}"> {{$item->name}}
                                                                    </div>
                                                                @endif
                                                            @endif
                                                        @endforeach
                                                    </div>
                                                </div>

                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        Attribute
                                                    </div>
                                                    <div class="panel-body">
                                                        @foreach($permission as $item)
                                                            @php
                                                                $check = 0;
                                                            @endphp
                                                            @foreach ($role->permissions as $ite)
                                                                @if ($item->id == $ite->id)
                                                                    @php
                                                                        $check = 1;
                                                                    @endphp
                                                                @endif
                                                            @endforeach
                                                            @if($item->key == 'attribute')
                                                                @if($check==1)
                                                                    <div class="col-lg-3">
                                                                        <input checked type="checkbox" name="permission[]" value="{{$item->id}}"> {{$item->name}}
                                                                    </div>
                                                                @else
                                                                    <div class="col-lg-3">
                                                                        <input type="checkbox" name="permission[]" value="{{$item->id}}"> {{$item->name}}
                                                                    </div>
                                                                @endif
                                                            @endif
                                                        @endforeach
                                                    </div>
                                                </div>

                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        Job
                                                    </div>
                                                    <div class="panel-body">
                                                        @foreach($permission as $item)
                                                            @php
                                                                $check = 0;
                                                            @endphp
                                                            @foreach ($role->permissions as $ite)
                                                                @if ($item->id == $ite->id)
                                                                    @php
                                                                        $check = 1;
                                                                    @endphp
                                                                @endif
                                                            @endforeach
                                                            @if($item->key == 'job')
                                                                @if($check==1)
                                                                    <div class="col-lg-3">
                                                                        <input checked type="checkbox" name="permission[]" value="{{$item->id}}"> {{$item->name}}
                                                                    </div>
                                                                @else
                                                                    <div class="col-lg-3">
                                                                        <input type="checkbox" name="permission[]" value="{{$item->id}}"> {{$item->name}}
                                                                    </div>
                                                                @endif
                                                            @endif
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-7 m-b-xs">
                                    </div>
                                    <div class="col-sm-5">
                                        <div class="col-sm-8 col-sm-offset-5">
                                            <button class="btn btn-white" type="reset">Reset</button>
                                            <button class="btn btn-primary" type="submit">Save changes</button>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>

                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection


