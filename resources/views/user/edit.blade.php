@extends('master.main')
@section('content')
    <div class=" text-center loginscreen animated fadeInDown">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-title">
                        <h5>Edit account</h5>
                        <a class="btn aa btn-primary " type="button" href="{{route('user.index')}}"><i class="fa fa-list"></i>&nbsp;List</a>
                    </div>
                    <div class="ibox-content">
                        <form id="form" method="post" action="{{ route('user.update',['id'=>$user->id]) }}" class="wizard-big">
                            @csrf
                            @method('PUT')
                            <fieldset>
                                <h2>Account Information</h2>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Username *</label>
                                            <input value="{{$user->username}}" id="username" name="username" type="text" class="form-control @error('username') is-invalid @enderror ">
                                            @error('username')
                                            <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>

                                        <div class="form-group">
                                            <label>Fullname *</label>
                                            <input value="{{$user->fullname}}" id="fullName" name="fullname" type="text" class="form-control @error('fullname') is-invalid @enderror ">
                                            @error('fullname')
                                            <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>

                                    </div>

                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <input value="admin"  name="type" type="hidden" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label>Role </label>
                                            <select class="form-control m-b @error('type') is-invalid @enderror " name="role_id" >
                                                @foreach($role as $item)
                                                    @php
                                                        $check = 0
                                                    @endphp
                                                    @if($item->id==$user->role_id)
                                                        <option selected value="{{$item->id}}">{{$item->name}}</option>
                                                    @else
                                                        <option value="{{$item->id}}">{{$item->name}}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                        @error('role')
                                        <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                        @enderror

                                        <div class="form-group">
                                            <label>Password *</label>
                                            <input id="password" name="passwords" type="password" class="form-control @error('passwords') is-invalid @enderror " >
                                            @error('passwords')
                                            <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>

                                        <div class="form-group">
                                            <label>Confirm Password *</label>
                                            <input id="confirm" name="confirm" type="password" class="form-control @error('confirm') is-invalid @enderror " >
                                            @error('confirm')
                                            <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>

                                    </div>
                                </div>
                            </fieldset>
                            <div class="form-group row">
                                <div class="col-sm-7 m-b-xs">
                                </div>
                                <div class="col-sm-5">
                                    <div class="col-sm-8 col-sm-offset-5">
                                        <button class="btn btn-white" type="reset">Reset</button>
                                        <button class="btn btn-primary" type="submit">Save changes</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection

