<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/admin', 'AdminController@index')->name('admin');

Route::group(['prefix' => 'user'], function () {
    Route::get('create','UserController@create')->middleware('can:create-admin')
        ->name('user.create');
    Route::post('store','UserController@store')
        ->name('user.store');
    Route::get('index','UserController@index')->middleware('can:view-admin')
        ->name('user.index');
    Route::get('/edit/{id}','UserController@edit')->middleware('can:edit-admin')
        ->name('user.edit');
    Route::put('/update/{id}','UserController@update')
        ->name('user.update');
    Route::get('user/{user}/delete','UserController@destroy')->middleware('can:delete-admin')
        ->name('user.destroy');
    Route::get('/search','UserController@search')->name('search.user');
});

Route::group(['prefix' => 'customer'], function () {
    Route::get('create','CustomerController@create')->middleware("can:create-customer")
        ->name('customer.create');
    Route::post('store','CustomerController@store')
        ->name('customer.store');
    Route::get('index','CustomerController@index')->middleware("can:view-customer")
        ->name('customer.index');
    Route::get('/edit/{id}','CustomerController@edit')->middleware("can:edit-customer")
        ->name('customer.edit');
    Route::put('/update/{id}','CustomerController@update')
        ->name('customer.update');
    Route::get('customer/{customer}/delete','CustomerController@destroy')->middleware("can:delete-customer")
        ->name('customer.destroy');
    Route::get('getDistrict','CustomerController@getDistrict')->name('getDistrict');
    Route::get('getWards','CustomerController@getWards')->name('getWards');
    Route::get('/search','CustomerController@search')->name('search.customer');
});

Route::group(['prefix' => 'employee'], function () {
    Route::get('create','EmployeeController@create')->middleware("can:create-employee")
        ->name('employee.create');
    Route::post('store','EmployeeController@store')
        ->name('employee.store');
    Route::get('index','EmployeeController@index')->middleware("can:view-employee")
        ->name('employee.index');
    Route::get('/edit/{id}','EmployeeController@edit')->middleware("can:edit-employee")
        ->name('employee.edit');
    Route::put('/update/{id}','EmployeeController@update')
        ->name('employee.update');
    Route::get('employee/{employee}/delete','EmployeeController@destroy')->middleware("can:delete-employee")
        ->name('employee.destroy');
    Route::get('/search','EmployeeController@search')->name('search.employee');
});

Route::group(['prefix' => 'role'], function () {
    Route::get('create','RoleController@create')->middleware('can:create-role')
        ->name('role.create');
    Route::post('store','RoleController@store')
        ->name('role.store');
    Route::get('index','RoleController@index')->middleware('can:view-role')
        ->name('role.index');
    Route::get('/edit/{id}','RoleController@edit')->middleware('can:edit-role')
        ->name('role.edit');
    Route::post('/update/{id}','RoleController@update')
        ->name('role.update');
    Route::delete('role/{role}/delete','RoleController@destroy')->middleware('can:delete-role')
        ->name('role.destroy');
    Route::get('/search','RoleController@search')->name('search.role');
});
Route::group(['prefix' => 'job'], function () {
    Route::get('create','JobController@create')->middleware('can:create-job')
        ->name('job.create');
    Route::post('store','JobController@store')
        ->name('job.store');
    Route::get('index','JobController@index')->middleware('can:view-job')
        ->name('job.index');
    Route::get('/edit/{id}','JobController@edit')->middleware('can:edit-job')
        ->name('job.edit');
    Route::get('/show/{id}','JobController@show')
        ->name('job.show');
    Route::post('/history/{id}','JobController@history')
        ->name('job.history');
    Route::post('/update/{id}','JobController@update')
        ->name('job.update');
    Route::get('job/{job}/delete','JobController@destroy')->middleware('can:delete-job')
        ->name('job.destroy');
    Route::get('getAttribute','JobController@getAttribute')->name('getAttribute');
    Route::get('getJobAttribute', 'JobController@getJobAttribute')->name('getJobAttribute');
});

Route::group(['prefix' => 'attribute'], function () {
    Route::get('create','AttributeController@create')->middleware('can:create-attribute')
        ->name('attribute.create');
    Route::post('store','AttributeController@store')
        ->name('attribute.store');
    Route::get('index','AttributeController@index')->middleware('can:view-attribute')
        ->name('attribute.index');
    Route::get('/edit/{id}','AttributeController@edit')->middleware('can:edit-attribute')
        ->name('attribute.edit');
    Route::post('/update/{id}','AttributeController@update')
        ->name('attribute.update');
    Route::get('attribute/{attribute}/delete','AttributeController@destroy')->middleware('can:delete-attribute')
        ->name('attribute.destroy');
});

Route::group(['prefix' => 'category'], function () {
    Route::get('create','CategoryController@create')->middleware('can:create-category')
        ->name('category.create');
    Route::post('store','CategoryController@store')
        ->name('category.store');
    Route::get('index','CategoryController@index')->middleware('can:view-category')
        ->name('category.index');
    Route::get('/edit/{id}','CategoryController@edit')->middleware('can:edit-category')
        ->name('category.edit');
    Route::post('/update/{id}','CategoryController@update')
        ->name('category.update');
    Route::get('category/{category}/delete','CategoryController@destroy')->middleware('can:delete-category')
        ->name('category.destroy');

});

Route::group(['prefix' => 'permission'], function () {
    Route::get('index','PermissionController@index')->middleware('can:view-permission')
        ->name('permission.index');
    Route::get('create','PermissionController@create')
        ->name('permission.create');
    Route::post('store','PermissionController@store')
        ->name('permission.store');
    Route::get('/search','PermissionController@search')->name('search.permission');
});

